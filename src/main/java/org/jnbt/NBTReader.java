// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.zip.InflaterInputStream;
import java.util.zip.GZIPInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.io.DataInputStream;
import java.io.Closeable;

public final class NBTReader implements Closeable
{
    private final DataInputStream is;
    private int nextType;
    private String nextName;
    private ArrayList<Integer> depthItems;
    private ArrayList<Integer> depthType;
    private int depth;
    
    @Deprecated
    public NBTReader(final InputStream is) throws IOException {
        this(is, NBTCompression.GZIP);
    }
    
    @Deprecated
    public NBTReader(final InputStream is, final boolean gzipped) throws IOException {
        this(is, gzipped ? NBTCompression.GZIP : NBTCompression.UNCOMPRESSED);
    }
    
    public NBTReader(final InputStream is, final NBTCompression compression) throws IOException {
        this.nextType = -1;
        this.nextName = null;
        this.depthItems = new ArrayList<Integer>();
        this.depthType = new ArrayList<Integer>();
        this.depth = -1;
        NBTCompression resolvedCompression;
        if (compression == NBTCompression.FROM_BYTE) {
            final int compressionByte = is.read();
            if (compressionByte < 0) {
                throw new EOFException();
            }
            resolvedCompression = NBTCompression.fromId(compressionByte);
        }
        else {
            resolvedCompression = compression;
        }
        switch (resolvedCompression) {
            case UNCOMPRESSED: {
                this.is = new DataInputStream(is);
                break;
            }
            case GZIP: {
                this.is = new DataInputStream(new GZIPInputStream(is));
                break;
            }
            case ZLIB: {
                this.is = new DataInputStream(new InflaterInputStream(is));
                break;
            }
            case FROM_BYTE: {
                throw new AssertionError((Object)"FROM_BYTE Should have been handled already");
            }
            default: {
                throw new AssertionError((Object)("[JNBT] Unimplemented " + NBTCompression.class.getSimpleName() + ": " + compression));
            }
        }
    }
    
    public String nextName() throws IOException {
        this.nextType();
        if (this.nextName != null) {
            return this.nextName;
        }
        if (this.nextType == 0) {
            this.nextName = "";
        }
        else {
            final int nameLength = this.is.readShort() & 0xFFFF;
            final byte[] nameBytes = new byte[nameLength];
            this.is.readFully(nameBytes);
            this.nextName = new String(nameBytes, NBTConstants.CHARSET);
        }
        return this.nextName;
    }
    
    public int nextType() throws IOException {
        if (this.nextType == -1) {
            this.nextType = (this.is.readByte() & 0xFF);
        }
        return this.nextType;
    }
    
    public byte nextByte() throws IOException {
        this.next();
        return this.is.readByte();
    }
    
    public short nextShort() throws IOException {
        this.next();
        return this.is.readShort();
    }
    
    public int nextInt() throws IOException {
        this.next();
        return this.is.readInt();
    }
    
    public long nextLong() throws IOException {
        this.next();
        return this.is.readLong();
    }
    
    public float nextFloat() throws IOException {
        this.next();
        return this.is.readFloat();
    }
    
    public double nextDouble() throws IOException {
        this.next();
        return this.is.readDouble();
    }
    
    public byte[] nextByteArray() throws IOException {
        this.next();
        final int length = this.is.readInt();
        final byte[] data = new byte[length];
        this.is.readFully(data);
        return data;
    }
    
    public String nextString() throws IOException {
        this.next();
        final int length = this.is.readShort();
        final byte[] bytes = new byte[length];
        this.is.readFully(bytes);
        return new String(bytes, NBTConstants.CHARSET);
    }
    
    public int[] nextIntArray() throws IOException {
        this.next();
        final int length = this.is.readInt();
        final int[] array = new int[length];
        for (int i = 0; i < length; ++i) {
            array[i] = this.is.readInt();
        }
        return array;
    }
    
    public long[] nextLongArray() throws IOException {
        this.next();
        final int length = this.is.readInt();
        final long[] array = new long[length];
        for (int i = 0; i < length; ++i) {
            array[i] = this.is.readLong();
        }
        return array;
    }
    
    private void next() throws IOException, IllegalStateException {
        this.nextName = null;
        int itemsLeft = (this.depth < 0) ? -1 : this.getRemainingItems();
        if (itemsLeft > 0) {
            --itemsLeft;
            this.depthItems.set(this.depth, itemsLeft);
            this.nextType = this.depthType.get(this.depth);
        }
        else {
            if (itemsLeft == 0) {
                throw new IllegalStateException("[JNBT] Attempted to read next element from a list with no remaining elements!");
            }
            this.nextType = -1;
        }
    }
    
    private int getRemainingItems() {
        return this.depthItems.get(this.depth);
    }
    
    public void beginObject() {
        this.nextName = null;
        this.nextType = -1;
        this.depthItems.add(-1);
        this.depthType.add(-1);
        ++this.depth;
    }
    
    public void beginArray() throws IOException {
        this.nextName = null;
        final int type = this.is.readByte();
        final int length = this.is.readInt();
        this.nextType = type;
        this.depthItems.add(length);
        this.depthType.add(type);
        ++this.depth;
    }
    
    public boolean hasNext() throws IOException, IllegalStateException {
        if (this.depth < 0) {
            throw new IllegalStateException("[JNBT] hasNext() cannot be called at the root level!");
        }
        final int itemsLeft = this.getRemainingItems();
        return itemsLeft > 0 || (itemsLeft == -1 && this.nextType() != 0);
    }
    
    public void endArray() throws IOException, IllegalStateException {
        final int itemsLeft = this.getRemainingItems();
        if (itemsLeft == -1) {
            throw new IllegalStateException("[JNBT] Attempted to end an object using endArray()!");
        }
        if (itemsLeft > 0) {
            throw new IllegalStateException("[JNBT] Attempted to end an array prematurely!");
        }
        --this.depth;
        this.depthItems.remove(this.depthItems.size() - 1);
        this.depthType.remove(this.depthType.size() - 1);
        this.next();
    }
    
    public void endObject() throws IOException, IllegalStateException {
        final int itemsLeft = this.getRemainingItems();
        if (itemsLeft != -1) {
            throw new IllegalStateException("[JNBT] Attempted to end an array using endObject()!");
        }
        if (this.nextType != 0) {
            throw new IllegalStateException("[JNBT] Attempted to end an object prematurely!");
        }
        --this.depth;
        this.depthItems.remove(this.depthItems.size() - 1);
        this.depthType.remove(this.depthType.size() - 1);
        this.next();
    }
    
    public void skipValue() throws IOException {
        this.skipValue(this.nextType);
        this.next();
    }
    
    private void skipValue(final int type) throws IOException {
        int length = 0;
        switch (type) {
            case 0: {
                length = 0;
                break;
            }
            case 1: {
                length = 1;
                break;
            }
            case 2: {
                length = 2;
                break;
            }
            case 3:
            case 5: {
                length = 4;
                break;
            }
            case 4:
            case 6: {
                length = 8;
                break;
            }
            case 7: {
                length = this.is.readInt();
                break;
            }
            case 8: {
                length = this.is.readUnsignedShort();
                break;
            }
            case 9: {
                final int listType = this.is.readByte();
                for (int listLength = this.is.readInt(), i = 0; i < listLength; ++i) {
                    this.skipValue(listType);
                }
                length = 0;
                break;
            }
            case 10: {
                for (int compType = this.is.readByte() & 0xFF; compType != 0; compType = (this.is.readByte() & 0xFF)) {
                    final int nameLength = this.is.readShort() & 0xFFFF;
                    final byte[] skip = new byte[nameLength];
                    this.is.readFully(skip);
                    this.skipValue(compType);
                }
                length = 0;
                break;
            }
            case 11: {
                length = this.is.readInt() * 4;
                break;
            }
            case 12: {
                length = this.is.readInt() * 8;
                break;
            }
            default: {
                throw new IOException("[JNBT] Invalid tag type: " + this.nextType + '.');
            }
        }
        while (length > 0) {
            final int delta = Math.min(length, 32768);
            final byte[] discard = new byte[delta];
            this.is.readFully(discard);
            length -= delta;
        }
    }
    
    @Override
    public void close() throws IOException {
        this.is.close();
    }
}
