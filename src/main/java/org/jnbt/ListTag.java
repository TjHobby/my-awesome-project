// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.Objects;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public final class ListTag extends Tag
{
    public static final int DEFAULT_INITIAL_CAPACITY = 4;
    private static final Pattern NEWLINE_PATTERN;
    private final NBTTagType type;
    private final List<Tag> value;
    
    public ListTag(final String name, final NBTTagType type, final List<Tag> value) {
        super(name);
        this.type = type;
        this.value = value;
    }
    
    public ListTag(final String name, final NBTTagType type) {
        this(name, type, new ArrayList<Tag>(4));
    }
    
    public NBTTagType getType() {
        return this.type;
    }
    
    @Override
    public List<Tag> getValue() {
        return this.value;
    }
    
    public int size() {
        return this.value.size();
    }
    
    public Tag get(final int index) {
        return this.value.get(index);
    }
    
    public void addTag(final Tag tag) {
        this.value.add(tag);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ListTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final ListTag listTag = (ListTag)obj;
        return Objects.equals(this.type, listTag.type) && Objects.equals(this.value, listTag.value);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.type, this.value);
    }
    
    @Override
    public String toString() {
        String joinedTags = this.value.stream().map(Tag::toString).map(s -> NEWLINE_PATTERN.matcher(s).replaceAll("\n   ")).collect(Collectors.joining("\n", "{\n", "\n}"));
        return getTagPrefixedToString(new String[] { this.type.getMojangName(), joinedTags });
    }
    
    static {
        NEWLINE_PATTERN = Pattern.compile("\n");
    }
}
