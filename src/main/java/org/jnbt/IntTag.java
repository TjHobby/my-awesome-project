// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.Objects;

public final class IntTag extends Tag
{
    private final int value;
    
    public IntTag(final String name, final int value) {
        super(name);
        this.value = value;
    }
    
    @Override
    public Integer getValue() {
        return this.value;
    }
    
    public int intValue() {
        return this.value;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IntTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final IntTag intTag = (IntTag)obj;
        return this.value == intTag.value;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }
}
