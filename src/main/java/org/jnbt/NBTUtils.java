// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.util.Collection;
import java.util.regex.Pattern;

public final class NBTUtils
{
    public static final Pattern NBT_FILE_NAME_PATTERN;
    
    private NBTUtils() {
        throw new AssertionError((Object)"Not instantiable");
    }
    
    public static List<CompoundTag> readNBTFiles(final Collection<File> files, final NBTCompression compression) throws IOException {
        final List<CompoundTag> result = new ArrayList<CompoundTag>(files.size());
        for (final File file : files) {
            final CompoundTag tag = readNBTFile(file, compression);
            result.add(tag);
        }
        return result;
    }
    
    public static CompoundTag readNBTFile(final File levelFile, final NBTCompression compression) throws IOException {
        try (final NBTInputStream in = new NBTInputStream(new BufferedInputStream(new FileInputStream(levelFile)), compression)) {
            return (CompoundTag)in.readTag();
        }
    }
    
    static {
        NBT_FILE_NAME_PATTERN = Pattern.compile("^(.*)\\.dat$");
    }
}
