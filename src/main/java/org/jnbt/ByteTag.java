// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.Objects;

public final class ByteTag extends Tag
{
    private final byte value;
    
    public ByteTag(final String name, final byte value) {
        super(name);
        this.value = value;
    }
    
    public ByteTag(final String name, final int value) {
        this(name, (byte)value);
    }
    
    @Override
    public Byte getValue() {
        return this.value;
    }
    
    public byte byteValue() {
        return this.value;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ByteTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final ByteTag byteTag = (ByteTag)obj;
        return this.value == byteTag.value;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }
}
