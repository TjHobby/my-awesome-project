// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.Objects;

public final class LongTag extends Tag
{
    private final long value;
    
    public LongTag(final String name, final long value) {
        super(name);
        this.value = value;
    }
    
    @Override
    public Long getValue() {
        return this.value;
    }
    
    public long longValue() {
        return this.value;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LongTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final LongTag longTag = (LongTag)obj;
        return this.value == longTag.value;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }
}
