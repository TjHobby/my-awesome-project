// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.Objects;

public final class FloatTag extends Tag
{
    private final float value;
    
    public FloatTag(final String name, final float value) {
        super(name);
        this.value = value;
    }
    
    @Override
    public Float getValue() {
        return this.value;
    }
    
    public float floatValue() {
        return this.value;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FloatTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final FloatTag floatTag = (FloatTag)obj;
        return Float.compare(floatTag.value, this.value) == 0;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }
}
