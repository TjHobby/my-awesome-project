// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.Objects;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public final class CompoundTag extends Tag
{
    public static final int DEFAULT_INITIAL_CAPACITY = 32;
    private static final Pattern NEWLINE_PATTERN;
    private final Map<String, Tag> value;
    
    public CompoundTag(final String name, final Map<String, Tag> value) {
        super(name);
        this.value = value;
    }
    
    public CompoundTag(final String name) {
        this(name, new HashMap<String, Tag>(32));
    }
    
    @Override
    public Map<String, Tag> getValue() {
        return this.value;
    }
    
    public void addTag(final Tag tag) {
        this.value.put(tag.getName(), tag);
    }
    
    public Tag getTag(final String key) {
        return this.value.get(key);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CompoundTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final CompoundTag compoundTag = (CompoundTag)obj;
        return Objects.equals(this.value, compoundTag.value);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }
    
    @Override
    public String toString() {
        final String joinedTags = this.value.values().stream().map((Function<? super Tag, ?>)Tag::toString).map(s -> CompoundTag.NEWLINE_PATTERN.matcher((String)s).replaceAll("\n   ")).collect(Collectors.joining("\n", "{\n", "\n}"));
        return this.getTagPrefixedToString(joinedTags);
    }
    
    static {
        NEWLINE_PATTERN = Pattern.compile("\n");
    }
}
