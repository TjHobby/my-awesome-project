// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.Objects;

public final class ShortTag extends Tag
{
    private final short value;
    
    public ShortTag(final String name, final short value) {
        super(name);
        this.value = value;
    }
    
    public ShortTag(final String name, final int value) {
        this(name, (short)value);
    }
    
    @Override
    public Short getValue() {
        return this.value;
    }
    
    public short shortValue() {
        return this.value;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ShortTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final ShortTag shortTag = (ShortTag)obj;
        return this.value == shortTag.value;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }
}
