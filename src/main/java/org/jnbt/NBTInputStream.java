// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.zip.InflaterInputStream;
import java.util.zip.GZIPInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.Closeable;

public final class NBTInputStream implements Closeable
{
    private final DataInputStream is;
    
    @Deprecated
    public NBTInputStream(final InputStream is) throws IOException {
        this(is, NBTCompression.GZIP);
    }
    
    @Deprecated
    public NBTInputStream(final InputStream is, final boolean gzipped) throws IOException {
        this(is, gzipped ? NBTCompression.GZIP : NBTCompression.UNCOMPRESSED);
    }
    
    @Deprecated
    public NBTInputStream(final DataInputStream is) {
        this.is = is;
    }
    
    public NBTInputStream(final InputStream is, final NBTCompression compression) throws IOException {
        NBTCompression resolvedCompression;
        if (compression == NBTCompression.FROM_BYTE) {
            final int compressionByte = is.read();
            if (compressionByte < 0) {
                throw new EOFException();
            }
            resolvedCompression = NBTCompression.fromId(compressionByte);
        }
        else {
            resolvedCompression = compression;
        }
        switch (resolvedCompression) {
            case UNCOMPRESSED: {
                this.is = new DataInputStream(is);
                break;
            }
            case GZIP: {
                this.is = new DataInputStream(new GZIPInputStream(is));
                break;
            }
            case ZLIB: {
                this.is = new DataInputStream(new InflaterInputStream(is));
                break;
            }
            case FROM_BYTE: {
                throw new AssertionError((Object)"FROM_BYTE Should have been handled already");
            }
            default: {
                throw new AssertionError((Object)("[JNBT] Unimplemented " + NBTCompression.class.getSimpleName() + ": " + compression));
            }
        }
    }
    
    public Tag readTag() throws IOException {
        return this.readTag(0);
    }
    
    private Tag readTag(final int depth) throws IOException {
        final int type = this.is.readByte() & 0xFF;
        String name;
        if (type == 0) {
            name = "";
        }
        else {
            final int nameLength = this.is.readShort() & 0xFFFF;
            final byte[] nameBytes = new byte[nameLength];
            this.is.readFully(nameBytes);
            name = new String(nameBytes, NBTConstants.CHARSET);
        }
        return this.readTagPayload(type, name, depth);
    }
    
    private Tag readTagPayload(final int type, final String name, final int depth) throws IOException {
        switch (type) {
            case 0: {
                return this.readEndTagPayload(depth);
            }
            case 1: {
                return this.readByteTagPayload(name);
            }
            case 2: {
                return this.readShortTagPayload(name);
            }
            case 3: {
                return this.readIntTagPayload(name);
            }
            case 4: {
                return this.readLongTagPayload(name);
            }
            case 5: {
                return this.readFloatTagPayload(name);
            }
            case 6: {
                return this.readDoubleTagPayload(name);
            }
            case 7: {
                return this.readByteArrayTagPayload(name);
            }
            case 8: {
                return this.readStringTagPayload(name);
            }
            case 9: {
                return this.readListTagPayload(name, depth);
            }
            case 10: {
                return this.readCompoundTagPayload(name, depth);
            }
            case 11: {
                return this.readIntArrayPayload(name);
            }
            case 12: {
                return this.readLongArrayPayload(name);
            }
            default: {
                throw new IOException("[JNBT] Invalid tag type: " + type + '.');
            }
        }
    }
    
    private Tag readEndTagPayload(final int depth) throws IOException {
        if (depth == 0) {
            throw new IOException("[JNBT] TAG_End found without a TAG_Compound/TAG_List tag preceding it.");
        }
        return new EndTag();
    }
    
    private Tag readByteTagPayload(final String name) throws IOException {
        return new ByteTag(name, this.is.readByte());
    }
    
    private Tag readShortTagPayload(final String name) throws IOException {
        return new ShortTag(name, this.is.readShort());
    }
    
    private Tag readIntTagPayload(final String name) throws IOException {
        return new IntTag(name, this.is.readInt());
    }
    
    private Tag readLongTagPayload(final String name) throws IOException {
        return new LongTag(name, this.is.readLong());
    }
    
    private Tag readFloatTagPayload(final String name) throws IOException {
        return new FloatTag(name, this.is.readFloat());
    }
    
    private Tag readDoubleTagPayload(final String name) throws IOException {
        return new DoubleTag(name, this.is.readDouble());
    }
    
    private Tag readByteArrayTagPayload(final String name) throws IOException {
        final int length = this.is.readInt();
        final byte[] bytes = new byte[length];
        this.is.readFully(bytes);
        return new ByteArrayTag(name, bytes);
    }
    
    private Tag readStringTagPayload(final String name) throws IOException {
        final int length = this.is.readShort();
        final byte[] bytes = new byte[length];
        this.is.readFully(bytes);
        final String string = new String(bytes, NBTConstants.CHARSET);
        return new StringTag(name, string);
    }
    
    private Tag readListTagPayload(final String name, final int depth) throws IOException {
        final int typeByte = this.is.readByte();
        final int length = this.is.readInt();
        final List<Tag> tagList = new ArrayList<Tag>(length);
        for (int i = 0; i < length; ++i) {
            final Tag tag = this.readTagPayload(typeByte, "", depth + 1);
            if (tag instanceof EndTag) {
                throw new IOException("[JNBT] TAG_End not permitted in a list.");
            }
            tagList.add(tag);
        }
        final NBTTagType tagType = NBTTagType.fromTypeByte(typeByte);
        return new ListTag(name, tagType, tagList);
    }
    
    private Tag readCompoundTagPayload(final String name, final int depth) throws IOException {
        final Map<String, Tag> tagMap = new HashMap<String, Tag>(32);
        while (true) {
            final Tag tag = this.readTag(depth + 1);
            if (tag instanceof EndTag) {
                break;
            }
            tagMap.put(tag.getName(), tag);
        }
        return new CompoundTag(name, tagMap);
    }
    
    private Tag readIntArrayPayload(final String name) throws IOException {
        final int length = this.is.readInt();
        final int[] ints = new int[length];
        for (int i = 0; i < length; ++i) {
            ints[i] = this.is.readInt();
        }
        return new IntArrayTag(name, ints);
    }
    
    private Tag readLongArrayPayload(final String name) throws IOException {
        final int length = this.is.readInt();
        final long[] longs = new long[length];
        for (int i = 0; i < length; ++i) {
            longs[i] = this.is.readLong();
        }
        return new LongArrayTag(name, longs);
    }
    
    @Override
    public void close() throws IOException {
        this.is.close();
    }
}
