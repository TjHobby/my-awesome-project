// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.Objects;

public final class DoubleTag extends Tag
{
    private final double value;
    
    public DoubleTag(final String name, final double value) {
        super(name);
        this.value = value;
    }
    
    @Override
    public Double getValue() {
        return this.value;
    }
    
    public double doubleValue() {
        return this.value;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DoubleTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final DoubleTag doubleTag = (DoubleTag)obj;
        return Double.compare(doubleTag.value, this.value) == 0;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }
}
