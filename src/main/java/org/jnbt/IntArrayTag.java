// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.IntFunction;
import java.util.Objects;
import java.util.Arrays;

public final class IntArrayTag extends Tag
{
    private final int[] value;

    public IntArrayTag(final String name, final int[] value) {
        super(name);
        this.value = value;
    }
    
    @Override
    public int[] getValue() {
        return this.value;
    }
    
    public int size() {
        return this.value.length;
    }
    
    public int get(final int index) {
        return this.value[index];
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IntArrayTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final IntArrayTag intArrayTag = (IntArrayTag)obj;
        return Arrays.equals(this.value, intArrayTag.value);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }
    
    @Override
    public String toString() {
        final String joinedValues = (String) Arrays.stream(this.value).mapToObj((IntFunction<?>)Integer::toString).collect((Collector)Collectors.joining(", ", "[", "]"));
        return this.getTagPrefixedToString(joinedValues);
    }
}
