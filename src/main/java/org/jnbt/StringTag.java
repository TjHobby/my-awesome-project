// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.Objects;

public final class StringTag extends Tag
{
    private final String value;
    
    public StringTag(final String name, final String value) {
        super(name);
        this.value = value;
    }
    
    @Override
    public String getValue() {
        return this.value;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StringTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final StringTag stringTag = (StringTag)obj;
        return Objects.equals(this.value, stringTag.value);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }
    
    @Override
    public String toString() {
        return this.getTagPrefixedToString("\"", this.value, "\"");
    }
}
