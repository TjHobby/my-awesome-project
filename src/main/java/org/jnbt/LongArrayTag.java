// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.LongFunction;
import java.util.Objects;
import java.util.Arrays;

public final class LongArrayTag extends Tag
{
    private final long[] value;
    
    public LongArrayTag(final String name, final long[] value) {
        super(name);
        this.value = value;
    }
    
    @Override
    public long[] getValue() {
        return this.value;
    }
    
    public int size() {
        return this.value.length;
    }
    
    public long get(final int index) {
        return this.value[index];
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LongArrayTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final LongArrayTag longArrayTag = (LongArrayTag)obj;
        return Arrays.equals(this.value, longArrayTag.value);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }
    
    @Override
    public String toString() {
        String joinedValues = Arrays.stream(this.value).<CharSequence>mapToObj(Long::toString).collect(Collectors.joining(", ", "[", "]"));
        return getTagPrefixedToString(new String[] { joinedValues });
    }
}
