// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

public enum NBTTagType
{
    TAG_END(0, (Class<? extends Tag>)EndTag.class, "TAG_End"), 
    TAG_BYTE(1, (Class<? extends Tag>)ByteTag.class, "TAG_Byte"), 
    TAG_SHORT(2, (Class<? extends Tag>)ShortTag.class, "TAG_Short"), 
    TAG_INT(3, (Class<? extends Tag>)IntTag.class, "TAG_Int"), 
    TAG_LONG(4, (Class<? extends Tag>)LongTag.class, "TAG_Long"), 
    TAG_FLOAT(5, (Class<? extends Tag>)FloatTag.class, "TAG_Float"), 
    TAG_DOUBLE(6, (Class<? extends Tag>)DoubleTag.class, "TAG_Double"), 
    TAG_BYTE_ARRAY(7, (Class<? extends Tag>)ByteArrayTag.class, "TAG_Byte_Array"), 
    TAG_STRING(8, (Class<? extends Tag>)StringTag.class, "TAG_String"), 
    TAG_LIST(9, (Class<? extends Tag>)ListTag.class, "TAG_List"), 
    TAG_COMPOUND(10, (Class<? extends Tag>)CompoundTag.class, "TAG_Compound"), 
    TAG_INT_ARRAY(11, (Class<? extends Tag>)IntArrayTag.class, "TAG_Int_Array"), 
    TAG_LONG_ARRAY(12, (Class<? extends Tag>)LongArrayTag.class, "TAG_Long_Array");
    
    private final int typeByte;
    private final Class<? extends Tag> tagClass;
    private final String mojangName;
    
    private NBTTagType(final int typeByte, final Class<? extends Tag> tagClass, final String mojangName) {
        this.typeByte = typeByte;
        this.tagClass = tagClass;
        this.mojangName = mojangName;
    }
    
    public int getTypeByte() {
        return this.typeByte;
    }
    
    public Class<? extends Tag> getTagClass() {
        return this.tagClass;
    }
    
    public String getMojangName() {
        return this.mojangName;
    }
    
    public static NBTTagType fromTypeByte(final int typeByte) {
        for (final NBTTagType value : values()) {
            if (value.typeByte == typeByte) {
                return value;
            }
        }
        throw new IllegalArgumentException("[JNBT] No " + NBTCompression.class.getSimpleName() + " enum constant with typeByte: " + typeByte);
    }
    
    public static NBTTagType fromTagClass(final Class<? extends Tag> tagClass) {
        for (final NBTTagType value : values()) {
            if (value.tagClass == tagClass) {
                return value;
            }
        }
        throw new IllegalArgumentException("[JNBT] No " + NBTCompression.class.getSimpleName() + " enum constant with tagClass: " + tagClass);
    }
    
    public static NBTTagType fromMojangName(final String mojangName) {
        for (final NBTTagType value : values()) {
            if (value.mojangName.equals(mojangName)) {
                return value;
            }
        }
        throw new IllegalArgumentException("[JNBT] No " + NBTCompression.class.getSimpleName() + " enum constant with mojangName: " + mojangName);
    }
}
