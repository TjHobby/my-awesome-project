// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

public final class EndTag extends Tag
{
    public EndTag() {
        super("");
    }
    
    @Override
    public Object getValue() {
        return null;
    }
    
    @Override
    public String toString() {
        return this.getTagPrefixedToString(new String[0]);
    }
}
