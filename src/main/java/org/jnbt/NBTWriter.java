// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.zip.InflaterOutputStream;
import java.util.zip.GZIPOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.io.DataOutputStream;
import java.io.Flushable;
import java.io.Closeable;

public final class NBTWriter implements Closeable, Flushable
{
    private final DataOutputStream os;
    private final int flags;
    private ArrayList<Integer> depthItems;
    private ArrayList<NBTTagType> depthType;
    private int depth;
    private boolean rootWritten;
    public static final int FLAG_ALLOW_ROOT_TAG_CHAINING = 1;
    public static final int FLAG_ALLOW_NON_COMPOUND_ROOT_TAG = 2;
    
    @Deprecated
    public NBTWriter(final OutputStream os) throws IOException {
        this(os, NBTCompression.GZIP);
    }
    
    @Deprecated
    public NBTWriter(final OutputStream os, final boolean gzipped) throws IOException {
        this(os, gzipped ? NBTCompression.GZIP : NBTCompression.UNCOMPRESSED);
    }
    
    public NBTWriter(final OutputStream os, final NBTCompression compression) throws IOException {
        this(os, compression, 0);
    }
    
    @Deprecated
    public NBTWriter(final OutputStream os, final NBTCompression compression, final int flags) throws IOException {
        this.depthItems = new ArrayList<Integer>();
        this.depthType = new ArrayList<NBTTagType>();
        this.depth = -1;
        this.rootWritten = false;
        this.flags = flags;
        switch (compression) {
            case UNCOMPRESSED: {
                this.os = new DataOutputStream(os);
                break;
            }
            case GZIP: {
                this.os = new DataOutputStream(new GZIPOutputStream(os));
                break;
            }
            case ZLIB: {
                this.os = new DataOutputStream(new InflaterOutputStream(os));
                break;
            }
            case FROM_BYTE: {
                throw new IllegalArgumentException(NBTCompression.FROM_BYTE.name() + " is only for reading.");
            }
            default: {
                throw new AssertionError((Object)("[JNBT] Unimplemented " + NBTCompression.class.getSimpleName() + ": " + compression));
            }
        }
    }
    
    private boolean hasFlag(final int flag) {
        return (this.flags & flag) == flag;
    }
    
    private void writeTagHeader(final String tagName, final NBTTagType type) throws IOException, IllegalStateException {
        if (this.depth < 0) {
            if (type != NBTTagType.TAG_COMPOUND && !this.hasFlag(2)) {
                throw new IOException("[JNBT] Invalid root tag type: " + type.getMojangName() + ".");
            }
            if (this.rootWritten && !this.hasFlag(1)) {
                throw new IOException("[JNBT] Only one root tag is permitted per file.");
            }
        }
        else {
            if (this.getRemainingItems() != -1 && type != this.depthType.get(this.depth)) {
                throw new IllegalStateException("[JNBT] Attempted to write a " + type.getMojangName() + " tag to a " + this.depthType.get(this.depth).getMojangName() + " list!");
            }
            if (this.getRemainingItems() == 0) {
                throw new IllegalStateException("[JNBT] Cannot write item to list; list size exceeded!");
            }
        }
        this.rootWritten = true;
        if (this.depth < 0 || this.getRemainingItems() == -1) {
            this.os.writeByte(type.getTypeByte());
            final byte[] tagNameBytes = tagName.getBytes(NBTConstants.CHARSET);
            this.os.writeShort(tagNameBytes.length);
            this.os.write(tagNameBytes);
        }
        if (this.depth >= 0 && this.getRemainingItems() != -1) {
            this.depthItems.set(this.depth, this.getRemainingItems() - 1);
        }
    }
    
    public void writeByte(final ByteTag b) throws IOException {
        this.writeByte(b.getName(), b.getValue());
    }
    
    public void writeByte(final String tagName, final byte b) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_BYTE);
        this.os.writeByte(b);
    }
    
    public void writeShort(final ShortTag s) throws IOException {
        this.writeShort(s.getName(), s.getValue());
    }
    
    public void writeShort(final String tagName, final short s) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_SHORT);
        this.os.writeShort(s);
    }
    
    public void writeInt(final IntTag i) throws IOException {
        this.writeInt(i.getName(), i.getValue());
    }
    
    public void writeInt(final String tagName, final int i) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_INT);
        this.os.writeInt(i);
    }
    
    public void writeLong(final LongTag l) throws IOException {
        this.writeLong(l.getName(), l.getValue());
    }
    
    public void writeLong(final String tagName, final long l) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_LONG);
        this.os.writeLong(l);
    }
    
    public void writeFloat(final FloatTag f) throws IOException {
        this.writeFloat(f.getName(), f.getValue());
    }
    
    public void writeFloat(final String tagName, final float f) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_FLOAT);
        this.os.writeFloat(f);
    }
    
    public void writeDouble(final DoubleTag d) throws IOException {
        this.writeDouble(d.getName(), d.getValue());
    }
    
    public void writeDouble(final String tagName, final double d) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_DOUBLE);
        this.os.writeDouble(d);
    }
    
    public void writeByteArray(final ByteArrayTag ba) throws IOException {
        this.writeByteArray(ba.getName(), ba.getValue());
    }
    
    public void writeByteArray(final String tagName, final byte[] ba) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_BYTE_ARRAY);
        this.os.writeInt(ba.length);
        this.os.write(ba);
    }
    
    public void writeString(final StringTag str) throws IOException {
        this.writeString(str.getName(), str.getValue());
    }
    
    public void writeString(final String tagName, final String str) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_STRING);
        final byte[] strBytes = str.getBytes(NBTConstants.CHARSET);
        this.os.writeShort(strBytes.length);
        this.os.write(strBytes);
    }
    
    public void writeIntArray(final IntArrayTag ia) throws IOException {
        this.writeIntArray(ia.getName(), ia.getValue());
    }
    
    public void writeIntArray(final String tagName, final int[] ia) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_INT_ARRAY);
        this.os.writeInt(ia.length);
        for (int i = 0; i < ia.length; ++i) {
            this.os.writeInt(ia[i]);
        }
    }
    
    public void writeLongArray(final LongArrayTag la) throws IOException {
        this.writeLongArray(la.getName(), la.getValue());
    }
    
    public void writeLongArray(final String tagName, final long[] la) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_LONG_ARRAY);
        this.os.writeInt(la.length);
        for (int i = 0; i < la.length; ++i) {
            this.os.writeLong(la[i]);
        }
    }
    
    private int getRemainingItems() {
        return this.depthItems.get(this.depth);
    }
    
    public void beginObject(final String tagName) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_COMPOUND);
        this.depthItems.add(-1);
        this.depthType.add(NBTTagType.TAG_END);
        ++this.depth;
    }
    
    public void beginArray(final String tagName, final NBTTagType listType, final int length) throws IOException {
        this.writeTagHeader(tagName, NBTTagType.TAG_LIST);
        this.os.writeByte(listType.getTypeByte());
        this.os.writeInt(length);
        this.depthItems.add(length);
        this.depthType.add(listType);
        ++this.depth;
    }
    
    public void endArray() throws IOException, IllegalStateException {
        final int itemsLeft = this.getRemainingItems();
        if (itemsLeft == -1) {
            throw new IllegalStateException("[JNBT] Attempted to end an object using endArray()!");
        }
        if (itemsLeft > 0) {
            throw new IllegalStateException("[JNBT] Attempted to end an array prematurely!");
        }
        --this.depth;
        this.depthItems.remove(this.depthItems.size() - 1);
        this.depthType.remove(this.depthType.size() - 1);
    }
    
    public void endObject() throws IOException, IllegalStateException {
        if (this.depth < 0) {
            throw new IllegalStateException("[JNBT] Attempted to end non-existent object above the root element!");
        }
        final int itemsLeft = this.getRemainingItems();
        if (itemsLeft != -1) {
            throw new IllegalStateException("[JNBT] Attempted to end an array using endObject()!");
        }
        this.os.writeByte(NBTTagType.TAG_END.getTypeByte());
        --this.depth;
        this.depthItems.remove(this.depthItems.size() - 1);
        this.depthType.remove(this.depthType.size() - 1);
    }
    
    @Override
    public void close() throws IOException {
        this.os.close();
    }
    
    @Override
    public void flush() throws IOException {
        this.os.flush();
    }
}
