// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

public enum NBTCompression
{
    UNCOMPRESSED(0), 
    GZIP(1), 
    ZLIB(2), 
    FROM_BYTE(-1);
    
    private final int compressionId;
    
    private NBTCompression(final int compressionId) {
        this.compressionId = compressionId;
    }
    
    public int getCompressionId() {
        return this.compressionId;
    }
    
    public static NBTCompression fromId(final int id) {
        for (final NBTCompression value : values()) {
            if (value.compressionId == id) {
                return value;
            }
        }
        throw new IllegalArgumentException("[JNBT] No " + NBTCompression.class.getSimpleName() + " enum constant with id: " + id);
    }
}
