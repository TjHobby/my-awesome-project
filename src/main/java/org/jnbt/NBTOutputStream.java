// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.List;
import java.util.Iterator;
import java.util.zip.InflaterOutputStream;
import java.util.zip.GZIPOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.Closeable;

public final class NBTOutputStream implements Closeable
{
    private final DataOutputStream os;
    
    @Deprecated
    public NBTOutputStream(final OutputStream os) throws IOException {
        this(os, NBTCompression.GZIP);
    }
    
    @Deprecated
    public NBTOutputStream(final OutputStream os, final boolean gzipped) throws IOException {
        this(os, gzipped ? NBTCompression.GZIP : NBTCompression.UNCOMPRESSED);
    }
    
    public NBTOutputStream(final OutputStream os, final NBTCompression compression) throws IOException {
        switch (compression) {
            case UNCOMPRESSED: {
                this.os = new DataOutputStream(os);
                break;
            }
            case GZIP: {
                this.os = new DataOutputStream(new GZIPOutputStream(os));
                break;
            }
            case ZLIB: {
                this.os = new DataOutputStream(new InflaterOutputStream(os));
                break;
            }
            case FROM_BYTE: {
                throw new IllegalArgumentException(NBTCompression.FROM_BYTE.name() + " is only for reading.");
            }
            default: {
                throw new AssertionError((Object)("[JNBT] Unimplemented " + NBTCompression.class.getSimpleName() + ": " + compression));
            }
        }
    }
    
    public void writeTag(final Tag tag) throws IOException {
        final NBTTagType type = NBTTagType.fromTagClass(tag.getClass());
        final byte[] nameBytes = tag.getName().getBytes(NBTConstants.CHARSET);
        if (type == NBTTagType.TAG_END) {
            throw new IOException("[JNBT] Named TAG_End not permitted.");
        }
        this.os.writeByte(type.getTypeByte());
        this.os.writeShort(nameBytes.length);
        this.os.write(nameBytes);
        this.writeTagPayload(tag);
    }
    
    private void writeTagPayload(final Tag tag) throws IOException {
        final NBTTagType type = NBTTagType.fromTagClass(tag.getClass());
        switch (type) {
            case TAG_END: {
                this.writeEndTagPayload((EndTag)tag);
                break;
            }
            case TAG_BYTE: {
                this.writeByteTagPayload((ByteTag)tag);
                break;
            }
            case TAG_SHORT: {
                this.writeShortTagPayload((ShortTag)tag);
                break;
            }
            case TAG_INT: {
                this.writeIntTagPayload((IntTag)tag);
                break;
            }
            case TAG_LONG: {
                this.writeLongTagPayload((LongTag)tag);
                break;
            }
            case TAG_FLOAT: {
                this.writeFloatTagPayload((FloatTag)tag);
                break;
            }
            case TAG_DOUBLE: {
                this.writeDoubleTagPayload((DoubleTag)tag);
                break;
            }
            case TAG_BYTE_ARRAY: {
                this.writeByteArrayTagPayload((ByteArrayTag)tag);
                break;
            }
            case TAG_STRING: {
                this.writeStringTagPayload((StringTag)tag);
                break;
            }
            case TAG_LIST: {
                this.writeListTagPayload((ListTag)tag);
                break;
            }
            case TAG_COMPOUND: {
                this.writeCompoundTagPayload((CompoundTag)tag);
                break;
            }
            case TAG_INT_ARRAY: {
                this.writeIntArrayTagPayload((IntArrayTag)tag);
                break;
            }
            case TAG_LONG_ARRAY: {
                this.writeLongArrayTagPayload((LongArrayTag)tag);
                break;
            }
            default: {
                throw new AssertionError((Object)("[JNBT] Unimplemented " + NBTTagType.class.getSimpleName() + ": " + type));
            }
        }
    }
    
    private void writeByteTagPayload(final ByteTag tag) throws IOException {
        this.os.writeByte(tag.getValue());
    }
    
    private void writeByteArrayTagPayload(final ByteArrayTag tag) throws IOException {
        final byte[] bytes = tag.getValue();
        this.os.writeInt(bytes.length);
        this.os.write(bytes);
    }
    
    private void writeCompoundTagPayload(final CompoundTag tag) throws IOException {
        for (final Tag childTag : tag.getValue().values()) {
            this.writeTag(childTag);
        }
        this.os.writeByte(0);
    }
    
    private void writeListTagPayload(final ListTag tag) throws IOException {
        final NBTTagType tagType = tag.getType();
        final List<Tag> tags = tag.getValue();
        final int length = tags.size();
        this.os.writeByte(tagType.getTypeByte());
        this.os.writeInt(length);
        for (final Tag t : tags) {
            this.writeTagPayload(t);
        }
    }
    
    private void writeStringTagPayload(final StringTag tag) throws IOException {
        final byte[] bytes = tag.getValue().getBytes(NBTConstants.CHARSET);
        this.os.writeShort(bytes.length);
        this.os.write(bytes);
    }
    
    private void writeDoubleTagPayload(final DoubleTag tag) throws IOException {
        this.os.writeDouble(tag.getValue());
    }
    
    private void writeFloatTagPayload(final FloatTag tag) throws IOException {
        this.os.writeFloat(tag.getValue());
    }
    
    private void writeLongTagPayload(final LongTag tag) throws IOException {
        this.os.writeLong(tag.getValue());
    }
    
    private void writeIntTagPayload(final IntTag tag) throws IOException {
        this.os.writeInt(tag.getValue());
    }
    
    private void writeShortTagPayload(final ShortTag tag) throws IOException {
        this.os.writeShort(tag.getValue());
    }
    
    private void writeIntArrayTagPayload(final IntArrayTag tag) throws IOException {
        final int[] ints = tag.getValue();
        this.os.writeInt(ints.length);
        for (final int i : ints) {
            this.os.writeInt(i);
        }
    }
    
    private void writeLongArrayTagPayload(final LongArrayTag tag) throws IOException {
        final long[] longs = tag.getValue();
        this.os.writeInt(longs.length);
        for (final long l : longs) {
            this.os.writeLong(l);
        }
    }
    
    private void writeEndTagPayload(final EndTag tag) {
    }
    
    @Override
    public void close() throws IOException {
        this.os.close();
    }
}
