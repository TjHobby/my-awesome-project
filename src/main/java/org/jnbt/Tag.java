// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.Objects;

public abstract class Tag
{
    private final String name;
    
    protected Tag(final String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public abstract Object getValue();
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tag)) {
            return false;
        }
        final Tag tag = (Tag)o;
        return Objects.equals(this.name, tag.name);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }
    
    @Override
    public String toString() {
        return this.getTagPrefixedToString(this.getValue().toString());
    }
    
    protected String getTagPrefixedToString(final String... valueParts) {
        final StringBuilder out = new StringBuilder(32);
        out.append(this.getClass().getSimpleName());
        out.append((this.name == null) ? "" : ("(\"" + this.name + "\")"));
        out.append(": ");
        for (final String valuePart : valueParts) {
            out.append(valuePart);
        }
        return out.toString();
    }
}
