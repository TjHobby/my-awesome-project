// 
// Decompiled by Procyon v0.5.36
// 

package org.jnbt;

import java.util.stream.IntStream;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.function.IntFunction;
import java.util.Objects;
import java.util.Arrays;

public final class ByteArrayTag extends Tag
{
    private final byte[] value;

    public ByteArrayTag(final String name, final byte[] value) {
        super(name);
        this.value = value;
    }

    @Override
    public byte[] getValue() {
        return this.value;
    }

    public int size() {
        return this.value.length;
    }

    public byte get(final int index) {
        return this.value[index];
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ByteArrayTag)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final ByteArrayTag byteArrayTag = (ByteArrayTag)obj;
        return Arrays.equals(this.value, byteArrayTag.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }

//    @Override
//    public String toString() {
//        final String joinedHexValues = byteArrayStream(this.value).mapToObj((IntFunction<?>)ByteArrayTag::toHexByte).collect(Collectors.joining(", ", "[", "]",));
//        return this.getTagPrefixedToString(joinedHexValues);
//    }

    private static IntStream byteArrayStream(final byte[] value) {
        return IntStream.range(0, value.length).map(i -> value[i]);
    }

    private static String toHexByte(final int b) {
        final String hex = Integer.toHexString(b & 0xFF);
        return (hex.length() == 1) ? ('0' + hex) : hex;
    }
}
