// 
// Decompiled by Procyon v0.5.36
// 

package org.hamcrest.core;

import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.Description;
import org.hamcrest.BaseMatcher;

public class IsAnything<T> extends BaseMatcher<T>
{
    private final String description;
    
    public IsAnything() {
        this("ANYTHING");
    }
    
    public IsAnything(final String description) {
        this.description = description;
    }
    
    public boolean matches(final Object o) {
        return true;
    }
    
    public void describeTo(final Description description) {
        description.appendText(this.description);
    }
    
    @Factory
    public static <T> Matcher<T> anything() {
        return new IsAnything<T>();
    }
    
    @Factory
    public static <T> Matcher<T> anything(final String description) {
        return new IsAnything<T>(description);
    }
    
    @Factory
    public static <T> Matcher<T> any(final Class<T> type) {
        return new IsAnything<T>();
    }
}
