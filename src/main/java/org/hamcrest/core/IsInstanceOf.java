// 
// Decompiled by Procyon v0.5.36
// 

package org.hamcrest.core;

import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.Description;
import org.hamcrest.BaseMatcher;

public class IsInstanceOf extends BaseMatcher<Object>
{
    private final Class<?> theClass;
    
    public IsInstanceOf(final Class<?> theClass) {
        this.theClass = theClass;
    }
    
    public boolean matches(final Object item) {
        return this.theClass.isInstance(item);
    }
    
    public void describeTo(final Description description) {
        description.appendText("an instance of ").appendText(this.theClass.getName());
    }
    
    @Factory
    public static Matcher<Object> instanceOf(final Class<?> type) {
        return new IsInstanceOf(type);
    }
}
