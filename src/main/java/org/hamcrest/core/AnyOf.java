// 
// Decompiled by Procyon v0.5.36
// 

package org.hamcrest.core;

import org.hamcrest.Factory;
import java.util.Arrays;
import org.hamcrest.SelfDescribing;
import org.hamcrest.Description;
import java.util.Iterator;
import org.hamcrest.Matcher;
import org.hamcrest.BaseMatcher;

public class AnyOf<T> extends BaseMatcher<T>
{
    private final Iterable<Matcher<? extends T>> matchers;
    
    public AnyOf(final Iterable<Matcher<? extends T>> matchers) {
        this.matchers = matchers;
    }
    
    public boolean matches(final Object o) {
        for (final Matcher<? extends T> matcher : this.matchers) {
            if (matcher.matches(o)) {
                return true;
            }
        }
        return false;
    }
    
    public void describeTo(final Description description) {
        description.appendList("(", " or ", ")", this.matchers);
    }
    
    @Factory
    public static <T> Matcher<T> anyOf(final Matcher<? extends T>... matchers) {
        return anyOf((Iterable<Matcher<? extends T>>)Arrays.asList(matchers));
    }
    
    @Factory
    public static <T> Matcher<T> anyOf(final Iterable<Matcher<? extends T>> matchers) {
        return new AnyOf<T>(matchers);
    }
}
