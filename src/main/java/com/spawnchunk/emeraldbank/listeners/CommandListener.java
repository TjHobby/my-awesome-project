// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.listeners;

import java.util.ArrayList;
import org.bukkit.event.EventPriority;
import org.bukkit.event.EventHandler;
import java.util.Iterator;
import java.util.Map;
import org.bukkit.command.CommandSender;
import org.bukkit.command.RemoteConsoleCommandSender;
import org.bukkit.command.ConsoleCommandSender;
import java.util.Arrays;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import com.spawnchunk.emeraldbank.config.Config;
import org.bukkit.entity.Player;
import java.util.List;
import java.util.HashMap;
import com.spawnchunk.emeraldbank.EmeraldBank;
import org.bukkit.event.server.TabCompleteEvent;
import org.bukkit.event.Listener;

public class CommandListener implements Listener
{
    @EventHandler(priority = EventPriority.NORMAL)
    public void onTabComplete(final TabCompleteEvent event) {
        if (event.isCancelled()) {
            return;
        }
        final String buffer = event.getBuffer();
        if (!buffer.startsWith("/bank ") && !buffer.startsWith("bank ") && !buffer.startsWith("/pay ") && !buffer.startsWith("pay ") && !buffer.startsWith("/deposit ") && !buffer.startsWith("deposit ") && !buffer.startsWith("/withdraw ") && !buffer.startsWith("withdraw ") && !buffer.startsWith("/balance ") && !buffer.startsWith("balance ") && !buffer.startsWith("/" + EmeraldBank.prefix + " ") && !buffer.startsWith(EmeraldBank.prefix + " ")) {
            return;
        }
        final CommandSender sender = event.getSender();
        final Map<String, List<String>> map = new HashMap<String, List<String>>();
        String cmd = "";
        if (sender instanceof Player) {
            final Player player = (Player)sender;
            if (buffer.matches("/bank pay [a-zA-Z0-9_]+ ")) {
                cmd = buffer;
                map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".pay", "true"));
            }
            else if (buffer.matches("/bank transfer [a-zA-Z0-9_]+ ")) {
                cmd = buffer;
                map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".transfer", "true"));
            }
            else if (buffer.matches("/bank add [a-zA-Z0-9_]+ ")) {
                cmd = buffer;
                map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".add", "true"));
            }
            else if (buffer.matches("/bank remove [a-zA-Z0-9_]+ ")) {
                cmd = buffer;
                map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".remove", "true"));
            }
            else if (buffer.matches("/bank set [a-zA-Z0-9_]+ ")) {
                cmd = buffer;
                map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".set", "true"));
            }
            else if (buffer.matches("/bank deposit ")) {
                cmd = buffer;
                map.put(LocaleStorage.translate("parameter.all", Config.locale), Arrays.asList(EmeraldBank.prefix + ".deposit", "true"));
                map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(".deposit", "true"));
            }
            else if (buffer.matches("/bank withdraw ")) {
                cmd = buffer;
                map.put(LocaleStorage.translate("parameter.all", Config.locale), Arrays.asList(EmeraldBank.prefix + ".deposit", "true"));
                map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(".withdraw", "true"));
            }
            else if (buffer.matches("/bank balance ") && player.hasPermission(EmeraldBank.prefix + ".balance.others")) {
                cmd = buffer;
                for (final Player p : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                    map.put(p.getName(), Arrays.asList(EmeraldBank.prefix + ".balance", "false"));
                }
            }
            else if (buffer.matches("/bank pay ")) {
                cmd = buffer;
                for (final Player p : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                    map.put(p.getName(), Arrays.asList(EmeraldBank.prefix + ".pay", "false"));
                }
            }
            else if (buffer.matches("/bank transfer ")) {
                cmd = buffer;
                for (final Player p : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                    map.put(p.getName(), Arrays.asList(EmeraldBank.prefix + ".transfer", "false"));
                }
            }
            else if (buffer.matches("/bank add ")) {
                cmd = buffer;
                for (final Player p : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                    map.put(p.getName(), Arrays.asList(EmeraldBank.prefix + ".add", "false"));
                }
            }
            else if (buffer.matches("/bank remove ")) {
                cmd = buffer;
                for (final Player p : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                    map.put(p.getName(), Arrays.asList(EmeraldBank.prefix + ".remove", "false"));
                }
            }
            else if (buffer.matches("/bank set ")) {
                cmd = buffer;
                for (final Player p : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                    map.put(p.getName(), Arrays.asList(EmeraldBank.prefix + ".set", "false"));
                }
            }
            else if (buffer.startsWith("/bank ")) {
                cmd = "/bank ";
                map.put(LocaleStorage.translate("command.deposit", Config.locale), Arrays.asList(EmeraldBank.prefix + ".deposit", "false"));
                map.put(LocaleStorage.translate("command.withdraw", Config.locale), Arrays.asList(EmeraldBank.prefix + ".withdraw", "false"));
                map.put(LocaleStorage.translate("command.transfer", Config.locale), Arrays.asList(EmeraldBank.prefix + ".transfer", "false"));
                map.put(LocaleStorage.translate("command.pay", Config.locale), Arrays.asList(EmeraldBank.prefix + ".pay", "false"));
                map.put(LocaleStorage.translate("command.add", Config.locale), Arrays.asList(EmeraldBank.prefix + ".add", "false"));
                map.put(LocaleStorage.translate("command.remove", Config.locale), Arrays.asList(EmeraldBank.prefix + ".remove", "false"));
                map.put(LocaleStorage.translate("command.set", Config.locale), Arrays.asList(EmeraldBank.prefix + ".set", "false"));
                map.put(LocaleStorage.translate("command.balance", Config.locale), Arrays.asList(EmeraldBank.prefix + ".balance", "false"));
                map.put(LocaleStorage.translate("command.baltop", Config.locale), Arrays.asList(EmeraldBank.prefix + ".baltop", "false"));
                map.put(LocaleStorage.translate("command.help", Config.locale), Arrays.asList(EmeraldBank.prefix + ".help", "false"));
            }
            else if (buffer.startsWith("/" + EmeraldBank.prefix + " ")) {
                cmd = "/" + EmeraldBank.prefix + " ";
                map.put(LocaleStorage.translate("command.reload", Config.locale), Arrays.asList(EmeraldBank.prefix + ".reload", "false"));
            }
        }
        else if (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender) {
            if (buffer.matches("bank add [a-zA-Z0-9_]+ ")) {
                cmd = buffer;
                map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".add", "true"));
            }
            else if (buffer.matches("bank remove [a-zA-Z0-9_]+ ")) {
                cmd = buffer;
                map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".remove", "true"));
            }
            else if (buffer.matches("bank set [a-zA-Z0-9_]+ ")) {
                cmd = buffer;
                map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".set", "true"));
            }
            else if (buffer.matches("bank balance ")) {
                cmd = buffer;
                for (final Player p2 : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                    map.put(p2.getName(), Arrays.asList(EmeraldBank.prefix + ".balance", "false"));
                }
            }
            else if (buffer.matches("bank add ")) {
                cmd = buffer;
                for (final Player p2 : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                    map.put(p2.getName(), Arrays.asList(EmeraldBank.prefix + ".add", "false"));
                }
            }
            else if (buffer.matches("bank remove ")) {
                cmd = buffer;
                for (final Player p2 : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                    map.put(p2.getName(), Arrays.asList(EmeraldBank.prefix + ".remove", "false"));
                }
            }
            else if (buffer.matches("bank set ")) {
                cmd = buffer;
                for (final Player p2 : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                    map.put(p2.getName(), Arrays.asList(EmeraldBank.prefix + ".set", "false"));
                }
            }
            else if (buffer.startsWith("bank ")) {
                cmd = "bank ";
                map.put(LocaleStorage.translate("command.add", Config.locale), Arrays.asList(EmeraldBank.prefix + ".add", "true"));
                map.put(LocaleStorage.translate("command.remove", Config.locale), Arrays.asList(EmeraldBank.prefix + ".remove", "true"));
                map.put(LocaleStorage.translate("command.set", Config.locale), Arrays.asList(EmeraldBank.prefix + ".set", "true"));
                map.put(LocaleStorage.translate("command.balance", Config.locale), Arrays.asList(EmeraldBank.prefix + ".balance", "true"));
                map.put(LocaleStorage.translate("command.baltop", Config.locale), Arrays.asList(EmeraldBank.prefix + ".baltop", "true"));
                map.put(LocaleStorage.translate("command.help", Config.locale), Arrays.asList(EmeraldBank.prefix + ".help", "true"));
            }
            else if (buffer.startsWith(EmeraldBank.prefix + " ")) {
                cmd = EmeraldBank.prefix + " ";
                map.put(LocaleStorage.translate("command.reload", Config.locale), Arrays.asList(EmeraldBank.prefix + ".reload", "false"));
            }
        }
        else if (buffer.matches("bank add [a-zA-Z0-9_]+ ")) {
            cmd = buffer;
            map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".add", "true"));
        }
        else if (buffer.matches("bank remove [a-zA-Z0-9_]+ ")) {
            cmd = buffer;
            map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".remove", "true"));
        }
        else if (buffer.matches("bank set [a-zA-Z0-9_]+ ")) {
            cmd = buffer;
            map.put("<" + LocaleStorage.translate("parameter.amount", Config.locale) + ">", Arrays.asList(EmeraldBank.prefix + ".set", "true"));
        }
        else if (buffer.matches("bank balance ")) {
            cmd = buffer;
            for (final Player p2 : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                map.put(p2.getName(), Arrays.asList(EmeraldBank.prefix + ".balance", "false"));
            }
        }
        else if (buffer.matches("bank add ")) {
            cmd = buffer;
            for (final Player p2 : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                map.put(p2.getName(), Arrays.asList(EmeraldBank.prefix + ".add", "false"));
            }
        }
        else if (buffer.matches("bank remove ")) {
            cmd = buffer;
            for (final Player p2 : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                map.put(p2.getName(), Arrays.asList(EmeraldBank.prefix + ".remove", "false"));
            }
        }
        else if (buffer.matches("bank set ")) {
            cmd = buffer;
            for (final Player p2 : EmeraldBank.plugin.getServer().getOnlinePlayers()) {
                map.put(p2.getName(), Arrays.asList(EmeraldBank.prefix + ".set", "false"));
            }
        }
        else if (buffer.startsWith("bank ")) {
            cmd = "bank ";
            map.put(LocaleStorage.translate("command.add", Config.locale), Arrays.asList(EmeraldBank.prefix + ".add", "false"));
            map.put(LocaleStorage.translate("command.remove", Config.locale), Arrays.asList(EmeraldBank.prefix + ".remove", "false"));
            map.put(LocaleStorage.translate("command.set", Config.locale), Arrays.asList(EmeraldBank.prefix + ".set", "false"));
            map.put(LocaleStorage.translate("command.balance", Config.locale), Arrays.asList(EmeraldBank.prefix + ".balance", "false"));
            map.put(LocaleStorage.translate("command.baltop", Config.locale), Arrays.asList(EmeraldBank.prefix + ".baltop", "false"));
        }
        final List<String> completions = this.buildCompletions(sender, buffer, cmd, map);
        event.setCompletions((List)completions);
    }
    
    public List<String> buildCompletions(final CommandSender sender, final String buffer, final String cmd, final Map<String, List<String>> map) {
        final List<String> completions = new ArrayList<String>();
        for (final String key : map.keySet()) {
            final List<String> opts = map.get(key);
            final String p = opts.get(0);
            final boolean parameter = Boolean.parseBoolean(opts.get(1));
            final String c = parameter ? cmd : (cmd + key);
            if (c.contains(buffer)) {
                if (sender instanceof Player) {
                    final Player player = (Player)sender;
                    if (!p.isEmpty() && !player.hasPermission(p) && !player.isOp()) {
                        continue;
                    }
                    completions.add(key);
                }
                else {
                    completions.add(key);
                }
            }
        }
        return completions;
    }
}
