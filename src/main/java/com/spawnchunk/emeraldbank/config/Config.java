// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.config;

import java.text.DecimalFormatSymbols;
import java.text.DecimalFormat;
import java.util.Locale;
import org.jetbrains.annotations.Nullable;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import java.util.logging.Level;
import com.spawnchunk.emeraldbank.EmeraldBank;
import java.text.NumberFormat;
import org.bukkit.configuration.file.FileConfiguration;

public class Config
{
    private FileConfiguration fc;
    private static final int config_version = 2;
    public static boolean debug;
    public static String locale;
    public static NumberFormat numberFormat;
    public static String decimal_format;
    public static String currency;
    public static String currency_plural;
    public static int max_balance;
    public static int max_withdraw;
    public static boolean fake_accounts;
    
    public Config() {
        this.fc = EmeraldBank.plugin.getConfig();
        this.parseConfig();
        this.getCurrency();
    }
    
    private void parseConfig() {
        final int version = this.fc.contains("configVersion") ? this.fc.getInt("configVersion") : 0;
        if (version < 2) {
            this.upgradeConfig();
        }
        if (this.fc.contains("debug")) {
            Config.debug = this.fc.getBoolean("debug");
        }
        if (this.fc.contains("locale")) {
            Config.locale = this.fc.getString("locale", "en_us");
        }
        if (Config.locale == null) {
            Config.locale = "en_us";
        }
        Config.numberFormat = this.getNumberFormat(Config.locale);
        if (this.fc.contains("decimal_format")) {
            Config.decimal_format = this.fc.getString("decimal_format");
            if (Config.decimal_format != null) {
                this.applyDecimalFormat(Config.decimal_format);
            }
        }
        if (this.fc.contains("balance.maximum")) {
            Config.max_balance = this.fc.getInt("balance.maximum");
        }
        if (this.fc.contains("withdraw.maximum")) {
            Config.max_withdraw = this.fc.getInt("withdraw.maximum");
        }
        if (this.fc.contains("fake_accounts")) {
            Config.fake_accounts = this.fc.getBoolean("fake_accounts");
        }
    }
    
    private void upgradeConfig() {
        EmeraldBank.logger.log(Level.WARNING, "Upgrading config file to the latest version");
        this.fc.options().copyDefaults(true);
        this.fc.set("configVersion", (Object)2);
        EmeraldBank.plugin.saveConfig();
    }
    
    public void reloadConfig() {
        if (Config.debug) {
            EmeraldBank.logger.info("Reloading configuration");
        }
        EmeraldBank.plugin.reloadConfig();
        this.fc = EmeraldBank.plugin.getConfig();
        this.parseConfig();
        LocaleStorage.reloadLocales();
        this.getCurrency();
    }
    
    public void getCurrency() {
        Config.currency = LocaleStorage.translate("currency", Config.locale);
        Config.currency_plural = LocaleStorage.translate("currency_plural", Config.locale);
    }
    
    public NumberFormat getNumberFormat(@Nullable final String locale) {
        Locale loc = Locale.US;
        if (locale != null && !locale.isEmpty() && locale.contains("_")) {
            final String language = locale.split("_")[0];
            final String country = locale.split("_")[1];
            loc = new Locale(language, country);
        }
        final NumberFormat numberFormat = NumberFormat.getInstance(loc);
        return numberFormat;
    }
    
    public void applyDecimalFormat(final String decimal_format) {
        if (!decimal_format.isEmpty()) {
            try {
                if (Config.numberFormat instanceof DecimalFormat) {
                    ((DecimalFormat)Config.numberFormat).applyPattern(decimal_format);
                    final DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat)Config.numberFormat).getDecimalFormatSymbols();
                    if (decimalFormatSymbols.getGroupingSeparator() == '@') {
                        Config.numberFormat.setGroupingUsed(false);
                    }
                }
            }
            catch (IllegalArgumentException ex) {}
            catch (NullPointerException ex2) {}
        }
    }
    
    static {
        Config.debug = false;
        Config.locale = "";
        Config.numberFormat = null;
        Config.decimal_format = "";
        Config.currency = "";
        Config.currency_plural = "";
        Config.max_balance = 0;
        Config.max_withdraw = 0;
        Config.fake_accounts = false;
    }
}
