// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.util;

import com.spawnchunk.emeraldbank.EmeraldBank;
import org.bukkit.OfflinePlayer;

public class PlayerUtil
{
    public static OfflinePlayer getOfflinePlayer(final String name) {
        final OfflinePlayer[] offlinePlayers;
        final OfflinePlayer[] players = offlinePlayers = EmeraldBank.plugin.getServer().getOfflinePlayers();
        for (final OfflinePlayer op : offlinePlayers) {
            if (name != null && name.equals(op.getName())) {
                return op;
            }
        }
        return null;
    }
}
