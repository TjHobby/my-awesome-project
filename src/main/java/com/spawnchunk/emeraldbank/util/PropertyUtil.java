// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.File;

public class PropertyUtil
{
    public static String getProperty(final File f, final String property) {
        final Properties pr = new Properties();
        try {
            final FileInputStream in = new FileInputStream(f);
            pr.load(in);
            return pr.getProperty(property);
        }
        catch (IOException ex) {
            return "";
        }
    }
}
