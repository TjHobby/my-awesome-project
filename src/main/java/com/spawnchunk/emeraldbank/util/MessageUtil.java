// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.util;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TranslatableComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.entity.Player;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import org.bukkit.command.CommandSender;
import com.spawnchunk.emeraldbank.config.Config;

public class MessageUtil
{
    public static String sectionSymbol(final String message) {
        String m = message.replaceAll("&#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])", "§x§$1§$2§$3§$4§$5§$6");
        m = m.replaceAll("&([0-9a-fk-or])", "§$1");
        return m;
    }
    
    public static String nocolor(final String message) {
        return message.replaceAll("§", "&").replaceAll("&#[&0-9a-fA-F]{12}", "").replaceAll("&[0-9a-fA-Fk-or]", "");
    }
    
    public static String populate(String message, final Object... args) {
        int pos = 0;
        for (final Object arg : args) {
            String value = "";
            if (arg instanceof String) {
                value = arg.toString();
            }
            if (arg instanceof Byte) {
                value = String.format("%d", arg);
            }
            if (arg instanceof Short) {
                value = String.format("%d", arg);
            }
            if (arg instanceof Integer) {
                value = String.format("%d", arg);
            }
            if (arg instanceof Long) {
                value = String.format("%dL", arg);
            }
            if (arg instanceof Float) {
                value = Config.numberFormat.format(arg);
            }
            if (arg instanceof Double) {
                value = Config.numberFormat.format(arg);
            }
            message = message.replace(String.format("{%d}", pos), value + "§r");
            ++pos;
        }
        return message;
    }
    
    public static void sendMessage(final CommandSender sender, final String key, final String locale) {
        String message = sectionSymbol(LocaleStorage.translate(key, locale));
        if (!(sender instanceof Player)) {
            message = nocolor(message);
        }
        if (!message.isEmpty()) {
            chatMessage(sender, translateItem(message));
        }
    }
    
    public static void sendMessage(final CommandSender sender, final String key, final String locale, final Object... args) {
        String message = sectionSymbol(populate(LocaleStorage.translate(key, locale), args));
        if (!(sender instanceof Player)) {
            message = nocolor(message);
        }
        if (!message.isEmpty()) {
            chatMessage(sender, translateItem(message));
        }
    }
    
    public static void sendMessage(final Player player, final String key, final String locale) {
        final String message = sectionSymbol(LocaleStorage.translate(key, locale));
        if (!message.isEmpty()) {
            chatMessage(player, translateItem(message));
        }
    }
    
    public static void sendMessage(final Player player, final String key, final String locale, final Object... args) {
        final String message = sectionSymbol(populate(LocaleStorage.translate(key, locale), args));
        if (!message.isEmpty()) {
            chatMessage(player, translateItem(message));
        }
    }
    
    private static BaseComponent[] translateItem(final String message) {
        final TextComponent tc = new TextComponent();
        tc.setColor(ChatColor.RESET);
        final ComponentBuilder builder = new ComponentBuilder((BaseComponent)tc);
        final String[] split;
        final String[] parts = split = message.split("::");
        for (final String part : split) {
            if (part.startsWith("{") && part.endsWith("}")) {
                final TranslatableComponent item = new TranslatableComponent(part.replaceAll("[{}]", ""), new Object[0]);
                builder.append((BaseComponent)item, ComponentBuilder.FormatRetention.NONE);
            }
            else {
                final BaseComponent[] components = TextComponent.fromLegacyText(part);
                builder.append(components, ComponentBuilder.FormatRetention.NONE);
            }
        }
        return builder.create();
    }
    
    private static void chatMessage(final CommandSender sender, final BaseComponent[] components) {
        sender.spigot().sendMessage(components);
    }
    
    private static void chatMessage(final Player player, final BaseComponent[] components) {
        player.spigot().sendMessage(ChatMessageType.CHAT, components);
    }
}
