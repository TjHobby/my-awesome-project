// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.modules;

import java.io.OutputStream;
import org.jnbt.NBTOutputStream;
import java.io.FileOutputStream;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.entity.Player;
import org.bukkit.OfflinePlayer;
import com.spawnchunk.emeraldbank.config.Config;
import org.jnbt.DoubleTag;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;
import org.bukkit.NamespacedKey;
import org.jnbt.Tag;
import org.bukkit.Material;
import org.jnbt.ByteTag;
import org.jnbt.StringTag;
import org.jnbt.ListTag;
import org.jnbt.CompoundTag;
import java.io.InputStream;
import org.jnbt.NBTInputStream;
import org.jnbt.NBTCompression;
import java.io.FileInputStream;
import org.bukkit.World;
import org.bukkit.Server;
import java.io.File;
import com.spawnchunk.emeraldbank.EmeraldBank;
import java.util.UUID;

public class PlayerData
{
    private static String getDataFileName(final UUID uuid) {
        final Server server = EmeraldBank.plugin.getServer();
        final World world = server.getWorld(EmeraldBank.level_name);
        if (world != null) {
            final String path = world.getWorldFolder().getPath();
            return path + File.separator + "playerdata" + File.separator + String.format("%s.dat", uuid.toString());
        }
        return null;
    }
    
    public static double valueOfInventory(final UUID uuid) {
        double amount = 0.0;
        final String fname = getDataFileName(uuid);
        if (fname != null) {
            try {
                final FileInputStream fis = new FileInputStream(fname);
                if (fis.available() > 0) {
                    try {
                        final NBTInputStream nbt = new NBTInputStream(fis, NBTCompression.GZIP);
                        final CompoundTag tag = (CompoundTag)nbt.readTag();
                        Tag tag2 = tag.getTag("Inventory");
                        if (tag2 == null) {
                            tag2 = new CompoundTag("Inventory");
                            tag.addTag(tag2);
                        }
                        if (tag2 instanceof ListTag) {
                            final ListTag list = (ListTag)tag2;
                            for (int i = 0; i < list.size(); ++i) {
                                final Tag tag3 = list.get(i);
                                if (tag3 instanceof CompoundTag) {
                                    final CompoundTag entry = (CompoundTag)tag3;
                                    String id = null;
                                    final Tag tag4 = entry.getTag("id");
                                    if (tag4 instanceof StringTag) {
                                        id = ((StringTag)tag4).getValue();
                                    }
                                    byte count = 0;
                                    final Tag tag5 = entry.getTag("Count");
                                    if (tag5 instanceof ByteTag) {
                                        count = ((ByteTag)tag5).getValue();
                                    }
                                    if (id != null) {
                                        final Material material = Material.matchMaterial(id);
                                        if (material == Material.EMERALD_BLOCK) {
                                            amount += count * 9;
                                        }
                                        if (material == Material.EMERALD) {
                                            amount += count;
                                        }
                                    }
                                }
                            }
                        }
                        nbt.close();
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                fis.close();
            }
            catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return amount;
    }
    
    public static double readBalance(final UUID uuid) {
        final Server server = EmeraldBank.plugin.getServer();
        final OfflinePlayer op = server.getOfflinePlayer(uuid);
        if (op.isOnline()) {
            final Player player = op.getPlayer();
            if (player != null) {
                final PersistentDataContainer pdc = player.getPersistentDataContainer();
                final NamespacedKey key = new NamespacedKey((Plugin)EmeraldBank.plugin, "balance");
                if (pdc.has(key, PersistentDataType.DOUBLE)) {
                    final Double balance = (Double)pdc.get(key, PersistentDataType.DOUBLE);
                    return (balance != null) ? balance : 0.0;
                }
                pdc.set(key, PersistentDataType.DOUBLE, 0.0);
            }
            return 0.0;
        }
        if (op.hasPlayedBefore()) {
            final String fname = getDataFileName(uuid);
            if (fname != null) {
                try {
                    final FileInputStream fis = new FileInputStream(fname);
                    if (fis.available() > 0) {
                        try {
                            final NBTInputStream nbt = new NBTInputStream(fis, NBTCompression.GZIP);
                            final CompoundTag tag = (CompoundTag)nbt.readTag();
                            double amount = 0.0;
                            Tag tag2 = tag.getTag("BukkitValues");
                            if (tag2 == null) {
                                tag2 = new CompoundTag("BukkitValues");
                                tag.addTag(tag2);
                            }
                            if (tag2 instanceof CompoundTag) {
                                final CompoundTag bank = (CompoundTag)tag2;
                                Tag tag3 = bank.getTag("emeraldbank:balance");
                                if (tag3 == null) {
                                    tag3 = new DoubleTag("emeraldbank:balance", 0.0);
                                    bank.addTag(tag3);
                                }
                                if (tag3 instanceof DoubleTag) {
                                    final DoubleTag balance2 = (DoubleTag)tag3;
                                    amount = balance2.getValue();
                                }
                            }
                            nbt.close();
                            return amount;
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    fis.close();
                }
                catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            return 0.0;
        }
        if (Config.fake_accounts) {
            final String name = op.getName();
            return FakeAccount.getBalance(name);
        }
        return 0.0;
    }
    
    public static void writeBalance(final UUID uuid, final Double amount) {
        final Server server = EmeraldBank.plugin.getServer();
        final OfflinePlayer op = server.getOfflinePlayer(uuid);
        if (op.isOnline()) {
            final Player player = op.getPlayer();
            if (player != null) {
                final PersistentDataContainer pdc = player.getPersistentDataContainer();
                final NamespacedKey key = new NamespacedKey((Plugin)EmeraldBank.plugin, "balance");
                pdc.set(key, PersistentDataType.DOUBLE, amount);
            }
        }
        else if (op.hasPlayedBefore()) {
            final String fname = getDataFileName(uuid);
            if (fname != null) {
                CompoundTag tag = null;
                try {
                    final FileInputStream fis = new FileInputStream(fname);
                    if (fis.available() > 0) {
                        try {
                            final NBTInputStream in = new NBTInputStream(fis, NBTCompression.GZIP);
                            tag = (CompoundTag)in.readTag();
                            in.close();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                    }
                    fis.close();
                    final FileOutputStream fos = new FileOutputStream(fname);
                    final NBTOutputStream out = new NBTOutputStream(fos, NBTCompression.GZIP);
                    if (tag != null) {
                        Tag tag2 = tag.getTag("BukkitValues");
                        if (tag2 == null) {
                            tag2 = new CompoundTag("BukkitValues");
                            tag.addTag(tag2);
                        }
                        if (tag2 instanceof CompoundTag) {
                            final CompoundTag bank = (CompoundTag)tag2;
                            final DoubleTag balance = new DoubleTag("emeraldbank:balance", amount);
                            bank.addTag(balance);
                            tag.addTag(bank);
                            out.writeTag(tag);
                        }
                    }
                    out.close();
                }
                catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        else if (Config.fake_accounts) {
            final String name = op.getName();
            FakeAccount.setBalance(name, amount);
        }
    }
}
