// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.modules;

import org.bukkit.Location;
import org.bukkit.World;
import java.util.ArrayList;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.Objects;
import java.util.List;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import java.util.Iterator;
import java.util.stream.Collector;
import java.util.function.Supplier;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Comparator;
import java.util.HashMap;
import com.spawnchunk.emeraldbank.EmeraldBank;
import com.spawnchunk.emeraldbank.util.MessageUtil;
import com.spawnchunk.emeraldbank.config.Config;
import org.bukkit.entity.Player;
import org.bukkit.command.CommandSender;
import java.util.UUID;
import org.bukkit.OfflinePlayer;

public class Balance
{
    public static double bankBalance(final OfflinePlayer op) {
        if (op == null) {
            return 0.0;
        }
        final UUID uuid = op.getUniqueId();
        return PlayerData.readBalance(uuid);
    }
    
    public static double balance(final OfflinePlayer op) {
        if (op == null) {
            return 0.0;
        }
        final UUID uuid = op.getUniqueId();
        final double on_hand = countAll(op);
        final double in_bank = PlayerData.readBalance(uuid);
        return on_hand + in_bank;
    }
    
    public static void balance(final CommandSender sender, final OfflinePlayer op) {
        if (op == null) {
            return;
        }
        boolean self = false;
        if (sender instanceof Player) {
            final Player player = (Player)sender;
            if (player.getUniqueId().equals(op.getUniqueId())) {
                self = true;
            }
        }
        final double on_hand = countAll(op);
        final double in_bank = PlayerData.readBalance(op.getUniqueId());
        final double balance = on_hand + in_bank;
        if (balance >= 0.0) {
            if (self) {
                MessageUtil.sendMessage(sender, "message.bank.balance", Config.locale, balance, (balance != 1.0) ? Config.currency_plural : Config.currency, on_hand, in_bank);
            }
            else {
                MessageUtil.sendMessage(sender, "message.bank.balance.others", Config.locale, op.getName(), balance, (balance != 1.0) ? Config.currency_plural : Config.currency, on_hand, in_bank);
            }
        }
        else if (self) {
            MessageUtil.sendMessage(sender, "warning.bank.balance", Config.locale);
        }
        else {
            MessageUtil.sendMessage(sender, "warning.bank.balance.others", Config.locale, op.getName());
        }
    }
    
    public static void baltop(final CommandSender sender) {
        final OfflinePlayer[] players = EmeraldBank.plugin.getServer().getOfflinePlayers();
        final Map<String, Double> balances = new HashMap<String, Double>();
        for (final OfflinePlayer op : players) {
            if (op != null) {
                final String key = op.getUniqueId().toString();
                final double on_hand = countAll(op);
                final double in_bank = PlayerData.readBalance(op.getUniqueId());
                final double balance = on_hand + in_bank;
                balances.put(key, balance);
            }
        }
        Comparator<? super Double> comparator = Comparator.reverseOrder();
        LinkedHashMap<String, Double> top = (LinkedHashMap<String, Double>)balances.entrySet().stream().sorted(Map.Entry.comparingByValue(comparator)).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        int entry = 0;
        int last_entry = 0;
        int last_balance = 0;
        MessageUtil.sendMessage(sender, "message.baltop", Config.locale);
        for (final String key2 : top.keySet()) {
            final UUID uuid = UUID.fromString(key2);
            final OfflinePlayer op2 = EmeraldBank.plugin.getServer().getOfflinePlayer(uuid);
            final int balance2 = top.get(key2).intValue();
            if (balance2 <= 0) {
                if (entry <= 0) {
                    MessageUtil.sendMessage(sender, "message.baltop.none", Config.locale);
                    break;
                }
                break;
            }
            else {
                ++entry;
                if (balance2 == last_balance) {
                    MessageUtil.sendMessage(sender, "message.baltop.entry", Config.locale, last_entry, op2.getName(), balance2);
                }
                else {
                    MessageUtil.sendMessage(sender, "message.baltop.entry", Config.locale, entry, op2.getName(), balance2);
                }
                if (entry >= 10) {
                    break;
                }
                last_balance = balance2;
                last_entry = entry;
            }
        }
    }
    
    public static double add(final OfflinePlayer op, final Double amount) {
        if (op == null) {
            return 0.0;
        }
        if (amount <= 0.0) {
            return 0.0;
        }
        final UUID uuid = op.getUniqueId();
        double in_bank = PlayerData.readBalance(op.getUniqueId());
        final double new_balance = in_bank + amount;
        if (Config.max_balance > 0 && new_balance >= Config.max_balance) {
            return -1.0;
        }
        in_bank += amount;
        PlayerData.writeBalance(uuid, in_bank);
        return amount;
    }
    
    public static void add(final CommandSender sender, final OfflinePlayer op, final Double amount) {
        if (op == null) {
            return;
        }
        if (amount <= 0.0) {
            return;
        }
        final UUID uuid = op.getUniqueId();
        final double in_bank = PlayerData.readBalance(op.getUniqueId());
        final double new_balance = in_bank + amount;
        if (amount >= 0.0) {
            if (Config.max_balance <= 0 || new_balance <= Config.max_balance) {
                PlayerData.writeBalance(uuid, new_balance);
                boolean self = false;
                if (sender instanceof Player && ((Player)sender).getUniqueId().equals(op.getUniqueId())) {
                    self = true;
                }
                if (op.isOnline()) {
                    if (self) {
                        MessageUtil.sendMessage(sender, "message.bank.deposit", Config.locale, amount, (amount != 1.0) ? Config.currency_plural : Config.currency);
                        if (Config.max_balance > 0 && amount == Config.max_balance) {
                            MessageUtil.sendMessage(sender, "warning.bank.balance.max", Config.locale);
                        }
                    }
                    else {
                        MessageUtil.sendMessage(sender, "message.bank.add", Config.locale, amount, (amount != 1.0) ? Config.currency_plural : Config.currency, op.getName());
                        MessageUtil.sendMessage(op.getPlayer(), "warning.bank.was_added", Config.locale, amount, (amount > 1.0) ? Config.currency_plural : Config.currency);
                        if (Config.max_balance > 0 && amount == Config.max_balance) {
                            MessageUtil.sendMessage(op.getPlayer(), "warning.bank.balance.max", Config.locale);
                        }
                    }
                }
                else {
                    MessageUtil.sendMessage(sender, "message.bank.add", Config.locale, amount, (amount != 1.0) ? Config.currency_plural : Config.currency, op.getName());
                }
            }
            else {
                MessageUtil.sendMessage(op.getPlayer(), "warning.bank.balance.max", Config.locale);
            }
        }
        else {
            MessageUtil.sendMessage(sender, "warning.bank.add", Config.locale);
        }
    }
    
    public static double remove(final OfflinePlayer op, final Double amount) {
        if (op == null) {
            return 0.0;
        }
        final UUID uuid = op.getUniqueId();
        final double on_hand = countAll(op);
        final double in_bank = PlayerData.readBalance(op.getUniqueId());
        final double balance = on_hand + in_bank;
        if (amount <= 0.0) {
            return 0.0;
        }
        if (in_bank >= amount) {
            PlayerData.writeBalance(uuid, in_bank - amount);
            return amount;
        }
        if (balance >= amount && op.isOnline()) {
            final Player player = op.getPlayer();
            PlayerData.writeBalance(uuid, 0.0);
            final double remain = amount - in_bank;
            removeSome(player, remain);
            return amount;
        }
        return -1.0;
    }
    
    public static void remove(final CommandSender sender, final OfflinePlayer op, final Double amount) {
        if (op == null) {
            return;
        }
        final UUID uuid = op.getUniqueId();
        double in_bank = PlayerData.readBalance(op.getUniqueId());
        if (amount >= 0.0 && in_bank >= amount) {
            in_bank -= amount;
            PlayerData.writeBalance(uuid, in_bank);
            boolean self = false;
            if (sender instanceof Player && ((Player)sender).getUniqueId().equals(op.getUniqueId())) {
                self = true;
            }
            if (op.isOnline()) {
                if (self) {
                    MessageUtil.sendMessage(sender, "message.bank.withdraw", Config.locale, amount, (amount != 1.0) ? Config.currency_plural : Config.currency);
                }
                else {
                    MessageUtil.sendMessage(op.getPlayer(), "warning.bank.was_removed", Config.locale, amount, (amount != 1.0) ? Config.currency_plural : Config.currency);
                }
            }
            return;
        }
        MessageUtil.sendMessage(sender, "warning.bank.remove", Config.locale);
    }
    
    public static void set(final CommandSender sender, final OfflinePlayer op, Double amount) {
        if (op == null) {
            return;
        }
        final UUID uuid = op.getUniqueId();
        amount = ((Config.max_balance > 0) ? Math.min(amount, Config.max_balance) : amount);
        if (amount >= 0.0) {
            PlayerData.writeBalance(uuid, amount);
            boolean self = false;
            if (sender instanceof Player && ((Player)sender).getUniqueId().equals(op.getUniqueId())) {
                self = true;
            }
            if (op.isOnline()) {
                if (self) {
                    MessageUtil.sendMessage(sender, "message.bank.set", Config.locale, op.getName(), amount, (amount != 1.0) ? Config.currency_plural : Config.currency);
                }
                else {
                    MessageUtil.sendMessage(sender, "message.bank.set", Config.locale, op.getName(), amount, (amount != 1.0) ? Config.currency_plural : Config.currency);
                    MessageUtil.sendMessage(op.getPlayer(), "warning.bank.was_set", Config.locale, amount, (amount != 1.0) ? Config.currency_plural : Config.currency);
                }
                if (Config.max_balance > 0 && amount == Config.max_balance) {
                    MessageUtil.sendMessage(op.getPlayer(), "warning.bank.balance.max", Config.locale);
                }
            }
            else {
                MessageUtil.sendMessage(sender, "message.bank.set", Config.locale, op.getName(), amount, (amount != 1.0) ? Config.currency_plural : Config.currency);
            }
            return;
        }
        MessageUtil.sendMessage(sender, "warning.bank.set", Config.locale);
    }
    
    public static void transfer(final CommandSender sender, final Player player, final OfflinePlayer op, Double amount) {
        if (op == null) {
            return;
        }
        if (op.getUniqueId().equals(player.getUniqueId())) {
            MessageUtil.sendMessage(sender, "warning.bank.transfer.self", Config.locale);
        }
        final double source = balance((OfflinePlayer)player);
        final double target = PlayerData.readBalance(op.getUniqueId());
        double balance = target + amount;
        balance = ((Config.max_balance > 0) ? Math.min(balance, Config.max_balance) : balance);
        amount = balance - target;
        if (source >= 0.0 && amount >= 0.0) {
            if (source >= amount) {
                remove((OfflinePlayer)player, amount);
                add(op, amount);
                MessageUtil.sendMessage(sender, "message.bank.transfer", Config.locale, amount, (amount != 1.0) ? Config.currency_plural : Config.currency, op.getName());
                if (op.isOnline()) {
                    MessageUtil.sendMessage(op.getPlayer(), "message.bank.receive", Config.locale, amount, (amount != 1.0) ? Config.currency_plural : Config.currency, player.getName());
                    if (balance == Config.max_balance) {
                        MessageUtil.sendMessage(op.getPlayer(), "warning.bank.balance.max", Config.locale);
                    }
                }
            }
            else {
                MessageUtil.sendMessage(player, "error.bank.insufficient", Config.locale, (amount != 1.0) ? Config.currency_plural : Config.currency);
            }
            return;
        }
        MessageUtil.sendMessage(player, "warning.bank.transfer", Config.locale);
    }
    
    public static void convert(final CommandSender sender, final Player player) {
        if (player == null) {
            return;
        }
        final PlayerInventory pi = player.getInventory();
        final ItemStack item = pi.getItemInMainHand();
        final double amount = valueOf(item);
        if (isCurrencyItem(item)) {
            pi.setItemInMainHand(new ItemStack(Material.AIR));
            add(sender, (OfflinePlayer)player, amount);
        }
        else {
            MessageUtil.sendMessage(player, "error.hand.item", Config.locale, (amount != 1.0) ? Config.currency_plural : Config.currency);
        }
    }
    
    public static void convertSome(final CommandSender sender, final Player player, final double amount) {
        if (player == null) {
            return;
        }
        final double on_hand = countAll((OfflinePlayer)player);
        if (on_hand >= amount) {
            removeSome(player, amount);
            add(sender, (OfflinePlayer)player, amount);
        }
        else {
            MessageUtil.sendMessage(player, "error.inventory.item", Config.locale, (amount != 1.0) ? Config.currency_plural : Config.currency);
        }
    }
    
    public static void removeSome(final Player player, final double amount) {
        if (player == null) {
            return;
        }
        double on_hand = 0.0;
        final PlayerInventory pi = player.getInventory();
        final int size = pi.getSize();
        for (int i = 0; i < size; ++i) {
            final ItemStack item = pi.getItem(i);
            if (isCurrencyItem(item)) {
                on_hand += valueOf(item);
            }
        }
        for (int i = 0; i < size; ++i) {
            final ItemStack item = pi.getItem(i);
            if (isCurrencyItem(item)) {
                pi.clear(i);
            }
        }
        final double remain = on_hand - amount;
        final List<ItemStack> items = currencyItemOf(remain);
        giveItems((CommandSender)player, player, items);
    }
    
    public static void convertAll(final CommandSender sender, final Player player) {
        if (player == null) {
            return;
        }
        final PlayerInventory pi = player.getInventory();
        final int size = pi.getSize();
        double amount = 0.0;
        for (int i = 0; i < size; ++i) {
            final ItemStack item = pi.getItem(i);
            if (isCurrencyItem(item)) {
                pi.setItem(i, new ItemStack(Material.AIR));
                amount += valueOf(item);
            }
        }
        if (amount > 0.0) {
            add(sender, (OfflinePlayer)player, amount);
        }
        else {
            MessageUtil.sendMessage(player, "error.inventory.item", Config.locale, (amount != 1.0) ? Config.currency_plural : Config.currency);
        }
    }
    
    public static double countAll(final OfflinePlayer op) {
        double amount = 0.0;
        if (op.isOnline()) {
            final Player player = op.getPlayer();
            final PlayerInventory pi = Objects.requireNonNull(player).getInventory();
            for (int size = pi.getSize(), i = 0; i < size; ++i) {
                final ItemStack item = pi.getItem(i);
                if (isCurrencyItem(item)) {
                    amount += valueOf(item);
                }
            }
        }
        else if (op.hasPlayedBefore()) {
            amount += PlayerData.valueOfInventory(op.getUniqueId());
        }
        return amount;
    }
    
    private static boolean isCurrencyItem(final ItemStack item) {
        if (item == null) {
            return false;
        }
        final Material material = item.getType();
        return (material == Material.EMERALD || material == Material.EMERALD_BLOCK) && !Objects.requireNonNull(item.getItemMeta()).hasEnchants() && !item.getItemMeta().hasLore();
    }
    
    private static double valueOf(final ItemStack item) {
        final Material material = item.getType();
        if (material == Material.EMERALD) {
            return item.getAmount();
        }
        if (material == Material.EMERALD_BLOCK) {
            return item.getAmount() * 9;
        }
        return 0.0;
    }
    
    public static void payout(final CommandSender sender, final Player player, final Double amount) {
        if (player == null) {
            return;
        }
        if (amount >= 0.0) {
            final double balance = PlayerData.readBalance(player.getUniqueId());
            if (balance >= amount) {
                remove((OfflinePlayer)player, amount);
                MessageUtil.sendMessage(sender, "message.bank.withdraw", Config.locale, amount, (amount != 1.0) ? Config.currency_plural : Config.currency);
                final List<ItemStack> items = currencyItemOf(amount);
                giveItems(sender, player, items);
                return;
            }
            MessageUtil.sendMessage(player, "error.bank.insufficient", Config.locale, (amount != 1.0) ? Config.currency_plural : Config.currency);
        }
        MessageUtil.sendMessage(sender, "warning.bank.withdraw", Config.locale);
    }
    
    private static List<ItemStack> currencyItemOf(final double amount) {
        final List<ItemStack> items = new ArrayList<ItemStack>();
        final double blocks = Math.floor(amount / 9.0);
        final double gems = Math.floor(amount % 9.0);
        final double stacks = Math.floor(blocks / 64.0);
        final double remain = blocks % 64.0;
        for (int i = 0; i < stacks; ++i) {
            items.add(new ItemStack(Material.EMERALD_BLOCK, 64));
        }
        items.add(new ItemStack(Material.EMERALD_BLOCK, (int)(short)remain));
        items.add(new ItemStack(Material.EMERALD, (int)(short)gems));
        return items;
    }
    
    private static void giveItems(final CommandSender sender, final Player player, final List<ItemStack> items) {
        if (player == null) {
            return;
        }
        final PlayerInventory pi = player.getInventory();
        final List<ItemStack> drops = new ArrayList<ItemStack>();
        for (final ItemStack item : items) {
            final HashMap<Integer, ItemStack> remain = (HashMap<Integer, ItemStack>)pi.addItem(new ItemStack[] { item });
            final ItemStack r = remain.get(0);
            if (r != null) {
                drops.add(r);
            }
        }
        if (!drops.isEmpty()) {
            final World world = player.getWorld();
            final Location location = player.getLocation();
            for (final ItemStack item2 : drops) {
                if (item2 != null) {
                    world.dropItem(location, item2);
                }
            }
            MessageUtil.sendMessage(sender, "error.inventory.full", Config.locale);
        }
    }
}
