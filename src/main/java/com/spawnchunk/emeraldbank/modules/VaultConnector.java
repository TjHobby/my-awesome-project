// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.modules;

import java.util.List;
import com.spawnchunk.emeraldbank.util.MessageUtil;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.OfflinePlayer;
import com.spawnchunk.emeraldbank.util.PlayerUtil;
import java.text.DecimalFormatSymbols;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import com.spawnchunk.emeraldbank.EmeraldBank;
import com.spawnchunk.emeraldbank.config.Config;
import net.milkbowl.vault.economy.Economy;

public class VaultConnector implements Economy
{
    public boolean isEnabled() {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::isEnabled() -> %s", EmeraldBank.econ ? "true" : "false"));
        }
        return EmeraldBank.econ;
    }
    
    public String getName() {
        final String result = EmeraldBank.plugin.getName();
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::getName() -> %s", result));
        }
        return result;
    }
    
    public boolean hasBankSupport() {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::hasBankSupport() -> %s", false));
        }
        return false;
    }
    
    public int fractionalDigits() {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::fractionalDigits() -> %s", 0));
        }
        return 0;
    }
    
    public String format(final double amount) {
        final Locale loc = new Locale(Config.locale);
        final NumberFormat f = NumberFormat.getInstance(loc);
        if (f instanceof DecimalFormat && !Config.decimal_format.isEmpty()) {
            try {
                ((DecimalFormat)f).applyPattern(Config.decimal_format);
                final DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat)f).getDecimalFormatSymbols();
                if (decimalFormatSymbols.getGroupingSeparator() == '@') {
                    f.setGroupingUsed(false);
                }
            }
            catch (IllegalArgumentException ex) {}
            catch (NullPointerException ex2) {}
        }
        final String formatted_amount = f.format(amount);
        final String result = String.format("%s %s", formatted_amount, (amount != 1.0) ? Config.currency_plural : Config.currency);
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::format(%.2f) -> %s", amount, result));
        }
        return result;
    }
    
    public String currencyNamePlural() {
        final String result = Config.currency_plural;
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::currencyNamePlural() -> %s", result));
        }
        return result;
    }
    
    public String currencyNameSingular() {
        final String result = Config.currency;
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::currencyNameSingular() -> %s", result));
        }
        return result;
    }
    
    public boolean hasAccount(final String player) {
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        final boolean result = (op != null && op.hasPlayedBefore()) || Config.fake_accounts;
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::hasAccount(%s) -> %s", player, result ? "true" : "false"));
        }
        return result;
    }
    
    public boolean hasAccount(final OfflinePlayer op) {
        final boolean result = op.hasPlayedBefore();
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::hasAccount(%s) -> %s", op.getUniqueId(), result ? "true" : "false"));
        }
        return result;
    }
    
    public boolean hasAccount(final String player, final String world) {
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        final boolean result = (op != null && op.hasPlayedBefore()) || Config.fake_accounts;
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::hasAccount(%s, %s) -> %s", player, world, result ? "true" : "false"));
        }
        return result;
    }
    
    public boolean hasAccount(final OfflinePlayer op, final String world) {
        final boolean result = op.hasPlayedBefore();
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::hasAccount(%s, %s) -> %s", op.getUniqueId(), world, result ? "true" : "false"));
        }
        return result;
    }
    
    public double getBalance(final String player) {
        double result = 0.0;
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        if (op != null) {
            result = Balance.balance(op);
        }
        else if (Config.fake_accounts) {
            result = FakeAccount.getBalance(player);
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::getBalance(%s) -> %.2f", player, result));
        }
        return result;
    }
    
    public double getBalance(final OfflinePlayer op) {
        double result = 0.0;
        if (op != null) {
            result = Balance.balance(op);
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::getBalance(%s) -> %.2f", (op != null) ? op.getUniqueId() : "(null)", result));
        }
        return result;
    }
    
    public double getBalance(final String player, final String world) {
        double result = 0.0;
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        if (op != null) {
            result = Balance.balance(op);
        }
        else if (Config.fake_accounts) {
            result = FakeAccount.getBalance(player);
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::getBalance(%s, %s) -> %.2f", player, world, result));
        }
        return result;
    }
    
    public double getBalance(final OfflinePlayer op, final String world) {
        double result = 0.0;
        if (op != null) {
            result = Balance.balance(op);
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::getBalance(%s, %s) -> %.2f", (op != null) ? op.getUniqueId() : "(null)", world, result));
        }
        return result;
    }
    
    public boolean has(final String player, final double amount) {
        boolean result = false;
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        if (op != null) {
            final double balance = Balance.balance(op);
            result = (balance >= amount);
        }
        else if (Config.fake_accounts) {
            result = FakeAccount.hasBalance(player, amount);
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::has(%s, %.2f) -> %s", player, amount, result ? "true" : "false"));
        }
        return result;
    }
    
    public boolean has(final OfflinePlayer op, final double amount) {
        boolean result = false;
        if (op != null) {
            final double balance = Balance.balance(op);
            result = (balance >= amount);
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::has(%s, %.2f) -> %s", (op != null) ? op.getUniqueId() : "(null)", amount, result ? "true" : "false"));
        }
        return result;
    }
    
    public boolean has(final String player, final String world, final double amount) {
        boolean result = false;
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        if (op != null) {
            final double balance = Balance.balance(op);
            result = (balance >= amount);
        }
        else if (Config.fake_accounts) {
            result = FakeAccount.hasBalance(player, amount);
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::has(%s, %s, %.2f) -> %s", player, world, amount, result ? "true" : "false"));
        }
        return result;
    }
    
    public boolean has(final OfflinePlayer op, final String world, final double amount) {
        boolean result = false;
        if (op != null) {
            final double balance = Balance.balance(op);
            result = (balance >= amount);
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::has(%s, %s, %.2f) -> %s", (op != null) ? op.getUniqueId() : "(null)", world, amount, result ? "true" : "false"));
        }
        return result;
    }
    
    public EconomyResponse withdrawPlayer(final String player, final double amount) {
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        EconomyResponse result;
        if (op != null) {
            final double balance = Balance.balance(op);
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.withdraw.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                final double new_balance = balance - amount;
                if (new_balance < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, MessageUtil.populate(LocaleStorage.translate("error.bank.insufficient", Config.locale), (amount != 1.0) ? Config.currency_plural : Config.currency));
                }
                else if (Balance.remove(op, amount) < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
                }
                else {
                    result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
                }
            }
        }
        else if (Config.fake_accounts) {
            final double balance = FakeAccount.getBalance(player);
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.withdraw.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                final double new_balance = balance - amount;
                if (new_balance < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, MessageUtil.populate(LocaleStorage.translate("error.bank.insufficient", Config.locale), (amount != 1.0) ? Config.currency_plural : Config.currency));
                }
                else {
                    FakeAccount.setBalance(player, new_balance);
                    result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
                }
            }
        }
        else {
            result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.player.unknown", Config.locale));
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::withdrawPlayer(%s, %.2f) -> %s: %s", player, amount, result.type.toString(), result.errorMessage));
        }
        return result;
    }
    
    public EconomyResponse withdrawPlayer(final OfflinePlayer op, final double amount) {
        EconomyResponse result;
        if (op != null) {
            final double balance = Balance.balance(op);
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.withdraw.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                final double new_balance = balance - amount;
                if (new_balance < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, MessageUtil.populate(LocaleStorage.translate("error.bank.insufficient", Config.locale), (amount != 1.0) ? Config.currency_plural : Config.currency));
                }
                else if (Balance.remove(op, amount) < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
                }
                else {
                    result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
                }
            }
        }
        else {
            result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.player.unknown", Config.locale));
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::withdrawPlayer(%s, %.2f) -> %s: %s", (op != null) ? op.getUniqueId() : "(null)", amount, result.type.toString(), result.errorMessage));
        }
        return result;
    }
    
    public EconomyResponse withdrawPlayer(final String player, final String world, final double amount) {
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        EconomyResponse result;
        if (op != null) {
            final double balance = Balance.balance(op);
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.withdraw.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                final double new_balance = balance - amount;
                if (new_balance < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, MessageUtil.populate(LocaleStorage.translate("error.bank.insufficient", Config.locale), (amount != 1.0) ? Config.currency_plural : Config.currency));
                }
                else if (Balance.remove(op, amount) < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
                }
                else {
                    result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
                }
            }
        }
        else if (Config.fake_accounts) {
            final double balance = FakeAccount.getBalance(player);
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.withdraw.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                final double new_balance = balance - amount;
                if (new_balance < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, MessageUtil.populate(LocaleStorage.translate("error.bank.insufficient", Config.locale), (amount != 1.0) ? Config.currency_plural : Config.currency));
                }
                else {
                    FakeAccount.setBalance(player, new_balance);
                    result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
                }
            }
        }
        else {
            result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.player.unknown", Config.locale));
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::withdrawPlayer(%s, %s, %.2f) -> %s: %s", player, world, amount, result.type.toString(), result.errorMessage));
        }
        return result;
    }
    
    public EconomyResponse withdrawPlayer(final OfflinePlayer op, final String world, final double amount) {
        EconomyResponse result;
        if (op != null) {
            final double balance = Balance.balance(op);
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.withdraw.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                final double new_balance = balance - amount;
                if (new_balance < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, MessageUtil.populate(LocaleStorage.translate("error.bank.insufficient", Config.locale), (amount != 1.0) ? Config.currency_plural : Config.currency));
                }
                else if (Balance.remove(op, amount) < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
                }
                else {
                    result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
                }
            }
        }
        else {
            result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.player.unknown", Config.locale));
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::withdrawPlayer(%s, %s, %.2f) -> %s: %s", (op != null) ? op.getUniqueId() : "(null)", world, amount, result.type.toString(), result.errorMessage));
        }
        return result;
    }
    
    public EconomyResponse depositPlayer(final String player, final double amount) {
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        EconomyResponse result;
        if (op != null) {
            final double balance = PlayerData.readBalance(op.getUniqueId());
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.deposit.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                final double new_balance = balance + amount;
                if (Config.max_balance > 0 && new_balance > Config.max_balance) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("warning.bank.balance.max", Config.locale));
                }
                else if (Balance.add(op, amount) < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("warning.bank.add", Config.locale));
                }
                else {
                    result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
                }
            }
        }
        else if (Config.fake_accounts) {
            final double balance = FakeAccount.getBalance(player);
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.deposit.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                double new_balance = balance + amount;
                new_balance = ((Config.max_balance > 0) ? Math.min(new_balance, Config.max_balance) : new_balance);
                FakeAccount.setBalance(player, new_balance);
                result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
            }
        }
        else {
            result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.player.unknown", Config.locale));
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::depositPlayer(%s, %.2f) -> %s: %s", player, amount, result.type.toString(), result.errorMessage));
        }
        return result;
    }
    
    public EconomyResponse depositPlayer(final OfflinePlayer op, final double amount) {
        EconomyResponse result;
        if (op != null) {
            final double balance = PlayerData.readBalance(op.getUniqueId());
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.deposit.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                final double new_balance = balance + amount;
                if (Config.max_balance > 0 && new_balance > Config.max_balance) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("warning.bank.balance.max", Config.locale));
                }
                else if (Balance.add(op, amount) < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("warning.bank.add", Config.locale));
                }
                else {
                    result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
                }
            }
        }
        else {
            result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.player.unknown", Config.locale));
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::depositPlayer(%s, %.2f) -> %s: %s", (op != null) ? op.getUniqueId() : "(null)", amount, result.type.toString(), result.errorMessage));
        }
        return result;
    }
    
    public EconomyResponse depositPlayer(final String player, final String world, final double amount) {
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        EconomyResponse result;
        if (op != null) {
            final double balance = PlayerData.readBalance(op.getUniqueId());
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.deposit.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                final double new_balance = balance + amount;
                if (Config.max_balance > 0 && new_balance > Config.max_balance) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("warning.bank.balance.max", Config.locale));
                }
                else if (Balance.add(op, amount) < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("warning.bank.add", Config.locale));
                }
                else {
                    result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
                }
            }
        }
        else if (Config.fake_accounts) {
            final double balance = FakeAccount.getBalance(player);
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.deposit.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                double new_balance = balance + amount;
                new_balance = ((Config.max_balance > 0) ? Math.min(new_balance, Config.max_balance) : new_balance);
                FakeAccount.setBalance(player, new_balance);
                result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
            }
        }
        else {
            result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.player.unknown", Config.locale));
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::depositPlayer(%s, %s, %.2f) -> %s: %s", player, world, amount, result.type.toString(), result.errorMessage));
        }
        return result;
    }
    
    public EconomyResponse depositPlayer(final OfflinePlayer op, final String world, final double amount) {
        EconomyResponse result;
        if (op != null) {
            final double balance = PlayerData.readBalance(op.getUniqueId());
            if (amount < 0.0) {
                result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.deposit.amount", Config.locale));
            }
            else if (balance < 0.0) {
                result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.bank.balance", Config.locale));
            }
            else {
                final double new_balance = balance + amount;
                if (Config.max_balance > 0 && new_balance > Config.max_balance) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("warning.bank.balance.max", Config.locale));
                }
                else if (Balance.add(op, amount) < 0.0) {
                    result = new EconomyResponse(amount, balance, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("warning.bank.add", Config.locale));
                }
                else {
                    result = new EconomyResponse(amount, new_balance, EconomyResponse.ResponseType.SUCCESS, "");
                }
            }
        }
        else {
            result = new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.FAILURE, LocaleStorage.translate("error.player.unknown", Config.locale));
        }
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::depositPlayer(%s, %s, %.2f) -> %s: %s", (op != null) ? op.getUniqueId() : "(null)", world, amount, result.type.toString(), result.errorMessage));
        }
        return result;
    }
    
    public EconomyResponse createBank(final String bank, final String player) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::createBank(%s, %s) -> (null)", bank, player));
        }
        return null;
    }
    
    public EconomyResponse createBank(final String bank, final OfflinePlayer op) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::createBank(%s, %s) -> (null)", bank, op.getUniqueId()));
        }
        return null;
    }
    
    public EconomyResponse deleteBank(final String bank) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::deleteBank(%s) -> (null)", bank));
        }
        return null;
    }
    
    public EconomyResponse bankBalance(final String bank) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::bankBalance(%s) -> (null)", bank));
        }
        return null;
    }
    
    public EconomyResponse bankHas(final String bank, final double amount) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::bankHas(%s, %.2f) -> (null)", bank, amount));
        }
        return null;
    }
    
    public EconomyResponse bankWithdraw(final String bank, final double amount) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::bankWithdraw(%s, %.2f) -> (null)", bank, amount));
        }
        return null;
    }
    
    public EconomyResponse bankDeposit(final String bank, final double amount) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::bankDeposit(%s, %.2f) -> (null)", bank, amount));
        }
        return null;
    }
    
    public EconomyResponse isBankOwner(final String bank, final String player) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::isBankOwner(%s, %s) -> (null)", bank, player));
        }
        return null;
    }
    
    public EconomyResponse isBankOwner(final String bank, final OfflinePlayer op) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::isBankOwner(%s, %s) -> (null)", bank, op.getUniqueId()));
        }
        return null;
    }
    
    public EconomyResponse isBankMember(final String bank, final String player) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::isBankMember(%s, %s) -> (null)", bank, player));
        }
        return null;
    }
    
    public EconomyResponse isBankMember(final String bank, final OfflinePlayer op) {
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::isBankMember(%s, %s) -> (null)", bank, op.getUniqueId()));
        }
        return null;
    }
    
    public List<String> getBanks() {
        if (Config.debug) {
            EmeraldBank.logger.info("VaultAPI::getBanks() -> (null)");
        }
        return null;
    }
    
    public boolean createPlayerAccount(final String player) {
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        final boolean result = (op != null && op.hasPlayedBefore()) || Config.fake_accounts;
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::createPlayerAccount(%s) -> %s", player, result ? "true" : "false"));
        }
        return result;
    }
    
    public boolean createPlayerAccount(final OfflinePlayer op) {
        final boolean result = op.hasPlayedBefore();
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::createPlayerAccount(%s) -> %s", op.getUniqueId(), result ? "true" : "false"));
        }
        return result;
    }
    
    public boolean createPlayerAccount(final String player, final String world) {
        final OfflinePlayer op = PlayerUtil.getOfflinePlayer(player);
        final boolean result = (op != null && op.hasPlayedBefore()) || Config.fake_accounts;
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::createPlayerAccount(%s, %s) -> %s", player, world, result ? "true" : "false"));
        }
        return result;
    }
    
    public boolean createPlayerAccount(final OfflinePlayer op, final String world) {
        final boolean result = op.hasPlayedBefore();
        if (Config.debug) {
            EmeraldBank.logger.info(String.format("VaultAPI::createPlayerAccount(%s, %s) -> %s", op.getUniqueId(), world, result ? "true" : "false"));
        }
        return result;
    }
}
