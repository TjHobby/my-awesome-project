// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.modules;

import org.bukkit.plugin.Plugin;
import com.spawnchunk.emeraldbank.EmeraldBank;
import org.bukkit.scheduler.BukkitRunnable;
import com.spawnchunk.emeraldbank.storage.AccountStorage;
import java.util.TreeMap;

public class FakeAccount
{
    private static TreeMap<String, Double> fake_accounts;
    
    private static void loadAccounts() {
        FakeAccount.fake_accounts = AccountStorage.loadAccountsFile();
    }
    
    private static void saveAccounts() {
        AccountStorage.saveAccountsFile(FakeAccount.fake_accounts);
    }
    
    private static void saveAccount(final String accountName) {
        if (FakeAccount.fake_accounts.containsKey(accountName)) {
            new BukkitRunnable() {
                public void run() {
                    AccountStorage.saveAccountsFile(FakeAccount.fake_accounts);
                }
            }.runTaskAsynchronously((Plugin)EmeraldBank.plugin);
        }
    }
    
    public static boolean hasBalance(final String accountName, final double amount) {
        if (FakeAccount.fake_accounts.containsKey(accountName)) {
            final double balance = FakeAccount.fake_accounts.get(accountName);
            return balance > amount;
        }
        return false;
    }
    
    public static double getBalance(final String accountName) {
        if (FakeAccount.fake_accounts.containsKey(accountName)) {
            return FakeAccount.fake_accounts.get(accountName);
        }
        FakeAccount.fake_accounts.put(accountName, 0.0);
        return 0.0;
    }
    
    public static void setBalance(final String accountName, final double balance) {
        if (FakeAccount.fake_accounts.containsKey(accountName)) {
            FakeAccount.fake_accounts.replace(accountName, balance);
        }
        else {
            FakeAccount.fake_accounts.put(accountName, balance);
        }
        saveAccount(accountName);
    }
    
    public static void onEnable() {
        loadAccounts();
    }
    
    public static void onDisable() {
        saveAccounts();
    }
    
    static {
        FakeAccount.fake_accounts = new TreeMap<String, Double>();
    }
}
