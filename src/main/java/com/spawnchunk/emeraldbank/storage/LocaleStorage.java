// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.storage;

import java.util.HashMap;
import com.spawnchunk.emeraldbank.config.Config;
import org.json.simple.JSONObject;
import java.io.Reader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.io.FileInputStream;
import org.json.simple.parser.JSONParser;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.logging.Level;
import com.spawnchunk.emeraldbank.EmeraldBank;
import java.nio.file.Path;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.CopyOption;
import java.nio.file.Paths;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.Map;

public class LocaleStorage
{
    public static final int locale_version = 11;
    public static final Map<String, TreeMap<String, String>> locales;
    
    public static void reloadLocales() {
        LocaleStorage.locales.clear();
        loadLocales();
    }
    
    public static void loadLocales() {
        installLocaleFile();
        final List<String> jsonFiles = listJsonFiles();
        for (final String filename : jsonFiles) {
            final String locale = containsLocale(filename);
            if (!locale.isEmpty()) {
                final TreeMap<String, String> map = loadLocaleFile(filename);
                LocaleStorage.locales.put(locale, map);
            }
        }
    }
    
    private static boolean backupFile(final File fn) {
        try {
            final Path source = Paths.get(fn.getAbsolutePath(), new String[0]);
            final Path dest = Paths.get(fn.getAbsolutePath() + ".backup", new String[0]);
            Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
        }
        catch (IOException e) {
            return false;
        }
        return true;
    }
    
    private static void installLocaleFile() {
        final String path = EmeraldBank.plugin.getDataFolder().getPath();
        final String filename = "en_us.json";
        final String fname = path + File.separator + filename;
        final File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        final File fn = new File(fname);
        if (!fn.exists()) {
            EmeraldBank.plugin.saveResource(filename, false);
        }
        else if (isOutdated(filename)) {
            if (backupFile(fn)) {
                EmeraldBank.plugin.saveResource(filename, true);
                EmeraldBank.logger.log(Level.WARNING, String.format("Warning! Locale file %s has been updated!", filename));
                EmeraldBank.logger.log(Level.WARNING, "A backup of the old file has been saved with .backup extension.");
                EmeraldBank.logger.log(Level.WARNING, "Please update the new file to include any customized translations.");
            }
            else {
                EmeraldBank.logger.log(Level.WARNING, "Error! Could not backup existing locale file");
            }
        }
    }
    
    private static List<String> listJsonFiles() {
        List<String> files = new ArrayList<String>();
        final String path = EmeraldBank.plugin.getDataFolder().getPath();
        final File dir = new File(path);
        if (dir.exists()) {
            final String[] list = dir.list((file, name) -> name.toLowerCase().endsWith(".json"));
            if (list != null) {
                files = Arrays.asList(list);
            }
        }
        return files;
    }
    
    private static boolean isOutdated(final String filename) {
        final String path = EmeraldBank.plugin.getDataFolder().getPath();
        final String fname = path + File.separator + filename;
        final File fn = new File(fname);
        if (fn.exists()) {
            final JSONParser parser = new JSONParser();
            try {
                final Object object = parser.parse(new InputStreamReader(new FileInputStream(fname), StandardCharsets.UTF_8));
                final JSONObject jsonObject = (JSONObject)object;
                final String version = (String) jsonObject.get("version");
                if (version == null) {
                    return true;
                }
                if (Integer.parseInt(version) >= 11) {
                    return false;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }
    
    private static String containsLocale(final String filename) {
        final String path = EmeraldBank.plugin.getDataFolder().getPath();
        final String fname = path + File.separator + filename;
        final File fn = new File(fname);
        if (fn.exists()) {
            final JSONParser parser = new JSONParser();
            try {
                final Object object = parser.parse(new InputStreamReader(new FileInputStream(fname), StandardCharsets.UTF_8));
                final JSONObject jsonObject = (JSONObject)object;
                final String language = (String)jsonObject.get("language.name");
                final String region = (String)jsonObject.get("language.region");
                final String locale = (String)jsonObject.get("language.code");
                if (filename.equals(locale + ".json")) {
                    if (Config.debug) {
                        EmeraldBank.logger.info(String.format("Found locale file %s", filename));
                    }
                    if (Config.debug) {
                        EmeraldBank.logger.info(String.format("language.name = %s", language));
                    }
                    if (Config.debug) {
                        EmeraldBank.logger.info(String.format("language.region = %s", region));
                    }
                    if (Config.debug) {
                        EmeraldBank.logger.info(String.format("language.code = %s", locale));
                    }
                    return locale;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }
    
    private static TreeMap<String, String> loadLocaleFile(final String filename) {
        final TreeMap<String, String> map = new TreeMap<String, String>();
        final String path = EmeraldBank.plugin.getDataFolder().getPath();
        final String fname = path + File.separator + filename;
        final File fn = new File(fname);
        if (fn.exists()) {
            if (isOutdated(filename)) {
                EmeraldBank.logger.log(Level.WARNING, String.format("Warning! Locale file %s is not the latest version!", filename));
                EmeraldBank.logger.log(Level.WARNING, "File should be updated to include and missing translations");
            }
            final JSONParser parser = new JSONParser();
            try {
                final Object object = parser.parse(new InputStreamReader(new FileInputStream(fname), StandardCharsets.UTF_8));
                final JSONObject jsonObject = (JSONObject)object;
                for (final Object obj : jsonObject.keySet()) {
                    final String key = (String)obj;
                    final String value = (String) jsonObject.get(key);
                    map.put(key, value);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }
    
    public static String translate(final String key, final String loc) {
        if (LocaleStorage.locales.containsKey(loc)) {
            if (LocaleStorage.locales.get(loc).containsKey(key)) {
                return LocaleStorage.locales.get(loc).get(key);
            }
            return String.format("<missing translation %s>", key);
        }
        else {
            final String fallback = "en_us";
            EmeraldBank.logger.info(String.format("Error! Missing locale %s, falling back to %s", loc, fallback));
            EmeraldBank.logger.info(String.format("Please check the locale in config.yml and that %s.json exists and has correct language.code", loc));
            Config.locale = fallback;
            if (LocaleStorage.locales.get(fallback).containsKey(key)) {
                return LocaleStorage.locales.get(fallback).get(key);
            }
            return String.format("<missing translation %s>", key);
        }
    }
    
    static {
        locales = new HashMap<String, TreeMap<String, String>>();
    }
}
