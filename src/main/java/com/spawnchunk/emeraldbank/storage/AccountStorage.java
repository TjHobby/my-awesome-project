// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.storage;

import java.io.OutputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.File;
import com.spawnchunk.emeraldbank.EmeraldBank;
import java.util.TreeMap;

public class AccountStorage
{
    public static TreeMap<String, Double> loadAccountsFile() {
        Object result = null;
        final TreeMap<String, Double> map = new TreeMap<String, Double>();
        final String path = EmeraldBank.plugin.getDataFolder().getPath();
        final String fname = path + File.separator + "fake_accounts.dat";
        final File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        final File fn = new File(fname);
        if (!fn.exists()) {
            try {
                fn.createNewFile();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            final FileInputStream fis = new FileInputStream(fname);
            if (fis.available() > 0) {
                try {
                    final ObjectInputStream ois = new ObjectInputStream(fis);
                    result = ois.readObject();
                    ois.close();
                }
                catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            fis.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if (result == null) {
            return map;
        }
        return (TreeMap<String, Double>)result;
    }
    
    public static void saveAccountsFile(final TreeMap<String, Double> map) {
        final String path = EmeraldBank.plugin.getDataFolder().getPath();
        final String fname = path + File.separator + "fake_accounts.dat";
        final File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        final File fn = new File(fname);
        if (!fn.exists()) {
            try {
                fn.createNewFile();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            final FileOutputStream fos = new FileOutputStream(fname);
            try {
                final ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(map);
                oos.flush();
                oos.close();
            }
            catch (Exception e2) {
                e2.printStackTrace();
            }
            fos.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
