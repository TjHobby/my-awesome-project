// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.commands;

import org.bukkit.OfflinePlayer;
import com.spawnchunk.emeraldbank.modules.Balance;
import com.spawnchunk.emeraldbank.util.PlayerUtil;
import com.spawnchunk.emeraldbank.EmeraldBank;
import com.spawnchunk.emeraldbank.util.MessageUtil;
import org.bukkit.entity.Player;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import com.spawnchunk.emeraldbank.config.Config;
import org.bukkit.command.Command;
import org.jetbrains.annotations.NotNull;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class PayCommand implements CommandExecutor
{
    public boolean onCommand(@NotNull final CommandSender sender, final Command cmd, @NotNull final String label, @NotNull final String[] args) {
        if (cmd.getName().equalsIgnoreCase(LocaleStorage.translate("command.pay", Config.locale))) {
            if (args.length == 2) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                if (player.hasPermission(EmeraldBank.prefix + ".transfer") || player.isOp()) {
                    final OfflinePlayer op = PlayerUtil.getOfflinePlayer(args[0].trim());
                    if (op == null) {
                        MessageUtil.sendMessage(sender, "error.player.unknown", Config.locale);
                        return true;
                    }
                    try {
                        final int amount = Integer.parseInt(args[1].trim());
                        if (amount > 0) {
                            Balance.transfer(sender, player, op, (double)amount);
                            return true;
                        }
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                    catch (NumberFormatException ex) {
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                }
            }
            MessageUtil.sendMessage(sender, "command.pay.usage", Config.locale);
            return true;
        }
        MessageUtil.sendMessage(sender, "warning.command.unknown", Config.locale);
        return true;
    }
}
