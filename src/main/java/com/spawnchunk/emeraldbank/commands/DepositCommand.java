// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.commands;

import com.spawnchunk.emeraldbank.modules.Balance;
import com.spawnchunk.emeraldbank.EmeraldBank;
import com.spawnchunk.emeraldbank.util.MessageUtil;
import org.bukkit.entity.Player;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import com.spawnchunk.emeraldbank.config.Config;
import org.bukkit.command.Command;
import org.jetbrains.annotations.NotNull;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class DepositCommand implements CommandExecutor
{
    public boolean onCommand(@NotNull final CommandSender sender, final Command cmd, @NotNull final String label, @NotNull final String[] args) {
        if (cmd.getName().equalsIgnoreCase(LocaleStorage.translate("command.deposit", Config.locale))) {
            if (args.length == 0) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                if (player.hasPermission(EmeraldBank.prefix + ".deposit") || player.isOp()) {
                    Balance.convert(sender, player);
                    return true;
                }
            }
            else if (args.length == 1 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.all", Config.locale))) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                if (player.hasPermission(EmeraldBank.prefix + ".deposit") || player.isOp()) {
                    Balance.convertAll(sender, player);
                    return true;
                }
            }
            else if (args.length == 1) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                Label_0314: {
                    if (!player.hasPermission(EmeraldBank.prefix + ".deposit")) {
                        if (!player.isOp()) {
                            break Label_0314;
                        }
                    }
                    try {
                        final int amount = Integer.parseInt(args[0].trim());
                        if (amount > 0) {
                            Balance.convertSome(sender, player, amount);
                            return true;
                        }
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                    catch (NumberFormatException ex) {
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                }
            }
            MessageUtil.sendMessage(sender, "command.deposit.usage", Config.locale);
            return true;
        }
        MessageUtil.sendMessage(sender, "warning.command.unknown", Config.locale);
        return true;
    }
}
