// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.commands;

import com.spawnchunk.emeraldbank.util.PlayerUtil;
import org.bukkit.OfflinePlayer;
import com.spawnchunk.emeraldbank.modules.Balance;
import com.spawnchunk.emeraldbank.EmeraldBank;
import com.spawnchunk.emeraldbank.util.MessageUtil;
import org.bukkit.entity.Player;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import com.spawnchunk.emeraldbank.config.Config;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;
import org.jetbrains.annotations.NotNull;

public class BalanceCommand implements CommandExecutor
{
    public boolean onCommand(@NotNull final CommandSender sender, final Command cmd, @NotNull final String label, @NotNull final String[] args) {
        if (cmd.getName().equalsIgnoreCase(LocaleStorage.translate("command.balance", Config.locale))) {
            if (args.length == 0) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                if (player.hasPermission(EmeraldBank.prefix + ".balance") || player.isOp()) {
                    Balance.balance(sender, (OfflinePlayer)player);
                    return true;
                }
            }
            else if (args.length == 1) {
                if (sender instanceof Player) {
                    final Player player = (Player)sender;
                    if (!player.hasPermission(EmeraldBank.prefix + ".balance.others") && !player.isOp()) {
                        return false;
                    }
                }
                final OfflinePlayer op = PlayerUtil.getOfflinePlayer(args[0].trim());
                if (op == null) {
                    MessageUtil.sendMessage(sender, "error.player.unknown", Config.locale);
                    return true;
                }
                Balance.balance(sender, op);
                return true;
            }
            MessageUtil.sendMessage(sender, "command.balance.usage", Config.locale);
            return false;
        }
        MessageUtil.sendMessage(sender, "warning.command.unknown", Config.locale);
        return true;
    }
}
