// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.commands;

import org.bukkit.OfflinePlayer;
import com.spawnchunk.emeraldbank.modules.Balance;
import com.spawnchunk.emeraldbank.EmeraldBank;
import com.spawnchunk.emeraldbank.util.MessageUtil;
import org.bukkit.entity.Player;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import com.spawnchunk.emeraldbank.config.Config;
import org.bukkit.command.Command;
import org.jetbrains.annotations.NotNull;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class WithdrawCommand implements CommandExecutor
{
    public boolean onCommand(@NotNull final CommandSender sender, final Command cmd, @NotNull final String label, @NotNull final String[] args) {
        if (cmd.getName().equalsIgnoreCase(LocaleStorage.translate("command.withdraw", Config.locale))) {
            if (args.length == 1 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.all", Config.locale))) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                if (player.hasPermission(EmeraldBank.prefix + ".withdraw") || player.isOp()) {
                    double amount = Balance.bankBalance((OfflinePlayer)player);
                    if (Config.max_withdraw > 0 && amount > Config.max_withdraw) {
                        amount = Config.max_withdraw;
                    }
                    Balance.payout(sender, player, amount);
                    return true;
                }
            }
            else if (args.length == 1) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                Label_0293: {
                    if (!player.hasPermission(EmeraldBank.prefix + ".withdraw")) {
                        if (!player.isOp()) {
                            break Label_0293;
                        }
                    }
                    try {
                        double amount = Integer.parseInt(args[0].trim());
                        if (amount > 0.0) {
                            if (Config.max_withdraw > 0 && amount > Config.max_withdraw) {
                                amount = Config.max_withdraw;
                            }
                            Balance.payout(sender, player, amount);
                            return true;
                        }
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                    catch (NumberFormatException ex) {
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                }
            }
            MessageUtil.sendMessage(sender, "command.withdraw.usage", Config.locale);
            return true;
        }
        MessageUtil.sendMessage(sender, "warning.command.unknown", Config.locale);
        return true;
    }
}
