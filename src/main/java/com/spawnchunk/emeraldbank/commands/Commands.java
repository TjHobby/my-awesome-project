// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.commands;

import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import com.spawnchunk.emeraldbank.config.Config;
import java.util.logging.Level;
import com.spawnchunk.emeraldbank.EmeraldBank;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;

public class Commands
{
    public static PluginCommand command_emeraldbank;
    public static PluginCommand command_bank;
    public static PluginCommand command_balance;
    public static PluginCommand command_baltop;
    public static PluginCommand command_deposit;
    public static PluginCommand command_pay;
    public static PluginCommand command_withdraw;
    
    private static PluginCommand registerCommand(final String command, final CommandExecutor commandExecutor) {
        final PluginCommand pluginCommand = EmeraldBank.plugin.getCommand(command);
        if (pluginCommand != null) {
            pluginCommand.setExecutor(commandExecutor);
        }
        else {
            EmeraldBank.logger.log(Level.WARNING, String.format("Could not register command %s", LocaleStorage.translate(command, Config.locale)));
        }
        return pluginCommand;
    }
    
    public static void registerCommands() {
        Commands.command_emeraldbank = registerCommand("emeraldbank", (CommandExecutor)new EmeraldBankCommand());
        Commands.command_bank = registerCommand("bank", (CommandExecutor)new BankCommand());
        Commands.command_baltop = registerCommand("baltop", (CommandExecutor)new BaltopCommand());
        Commands.command_balance = registerCommand("balance", (CommandExecutor)new BalanceCommand());
        Commands.command_deposit = registerCommand("deposit", (CommandExecutor)new DepositCommand());
        Commands.command_pay = registerCommand("pay", (CommandExecutor)new PayCommand());
        Commands.command_withdraw = registerCommand("withdraw", (CommandExecutor)new WithdrawCommand());
    }
}
