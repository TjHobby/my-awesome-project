// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.commands;

import com.spawnchunk.emeraldbank.util.PlayerUtil;
import org.bukkit.OfflinePlayer;
import com.spawnchunk.emeraldbank.modules.Balance;
import com.spawnchunk.emeraldbank.EmeraldBank;
import com.spawnchunk.emeraldbank.util.MessageUtil;
import org.bukkit.entity.Player;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import com.spawnchunk.emeraldbank.config.Config;
import org.bukkit.command.Command;
import org.jetbrains.annotations.NotNull;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class BankCommand implements CommandExecutor
{
    public boolean onCommand(@NotNull final CommandSender sender, final Command cmd, @NotNull final String label, @NotNull final String[] args) {
        if (cmd.getName().equalsIgnoreCase(LocaleStorage.translate("command.bank", Config.locale))) {
            if (args.length == 1 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.deposit", Config.locale))) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                if (player.hasPermission(EmeraldBank.prefix + ".deposit") || player.isOp()) {
                    Balance.convert(sender, player);
                    return true;
                }
            }
            else if (args.length == 2 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.deposit", Config.locale)) && args[1].equalsIgnoreCase(LocaleStorage.translate("command.all", Config.locale))) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                if (player.hasPermission(EmeraldBank.prefix + ".deposit") || player.isOp()) {
                    Balance.convertAll(sender, player);
                    return true;
                }
            }
            else if (args.length == 2 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.deposit", Config.locale))) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                Label_0369: {
                    if (!player.hasPermission(EmeraldBank.prefix + ".deposit")) {
                        if (!player.isOp()) {
                            break Label_0369;
                        }
                    }
                    try {
                        final int amount = Integer.parseInt(args[1].trim());
                        if (amount > 0) {
                            Balance.convertSome(sender, player, amount);
                            return true;
                        }
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                    catch (NumberFormatException ex) {
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                }
            }
            else if (args.length == 2 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.withdraw", Config.locale)) && args[1].equalsIgnoreCase(LocaleStorage.translate("command.all", Config.locale))) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                if (player.hasPermission(EmeraldBank.prefix + ".withdraw") || player.isOp()) {
                    double amount2 = Balance.bankBalance(player);
                    if (Config.max_withdraw > 0 && amount2 > Config.max_withdraw) {
                        amount2 = Config.max_withdraw;
                    }
                    Balance.payout(sender, player, amount2);
                    return true;
                }
            }
            else if (args.length == 2 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.withdraw", Config.locale))) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                Label_0694: {
                    if (!player.hasPermission(EmeraldBank.prefix + ".withdraw")) {
                        if (!player.isOp()) {
                            break Label_0694;
                        }
                    }
                    try {
                        double amount2 = Integer.parseInt(args[1].trim());
                        if (amount2 > 0.0) {
                            if (Config.max_withdraw > 0 && amount2 > Config.max_withdraw) {
                                amount2 = Config.max_withdraw;
                            }
                            Balance.payout(sender, (Player)sender, amount2);
                          //  Balance.payout(sender, player, amount2);
                            return true;
                        }
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                    catch (NumberFormatException ex) {
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                }
            }
            else if (args.length == 3 && (args[0].equalsIgnoreCase(LocaleStorage.translate("command.transfer", Config.locale)) || args[0].equalsIgnoreCase(LocaleStorage.translate("command.pay", Config.locale)))) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                if (player.hasPermission(EmeraldBank.prefix + ".transfer") || player.hasPermission(EmeraldBank.prefix + ".pay") || player.isOp()) {
                    final OfflinePlayer op = PlayerUtil.getOfflinePlayer(args[1].trim());
                    if (op == null) {
                        MessageUtil.sendMessage(sender, "error.player.unknown", Config.locale);
                        return true;
                    }
                    try {
                        final int amount3 = Integer.parseInt(args[2].trim());
                        if (amount3 > 0) {
                            Balance.transfer(sender, player, op, (double)amount3);
                            return true;
                        }
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                    catch (NumberFormatException ex2) {
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                }
            }
            else if (args.length == 0 || (args.length == 1 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.balance", Config.locale)))) {
                if (!(sender instanceof Player)) {
                    MessageUtil.sendMessage(sender, "console.command.player_only", Config.locale);
                    return true;
                }
                final Player player = (Player)sender;
                if (player.hasPermission(EmeraldBank.prefix + ".balance") || player.isOp()) {
                    Balance.balance(sender, player);
                    return true;
                }
            }
            else if (args.length == 2 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.balance", Config.locale))) {
                if (sender instanceof Player) {
                    final Player player = (Player)sender;
                    if (!player.hasPermission(EmeraldBank.prefix + ".balance.others") && !player.isOp()) {
                        return false;
                    }
                }
                final OfflinePlayer op2 = PlayerUtil.getOfflinePlayer(args[1].trim());
                if (op2 == null) {
                    MessageUtil.sendMessage(sender, "error.player.unknown", Config.locale);
                    return true;
                }
                Balance.balance(sender, op2);
                return true;
            }
            else {
                if (args.length == 1 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.baltop", Config.locale))) {
                    if (sender instanceof Player) {
                        final Player player = (Player)sender;
                        if (!player.hasPermission(EmeraldBank.prefix + ".baltop") && !player.isOp()) {
                            return false;
                        }
                    }
                    Balance.baltop(sender);
                    return true;
                }
                if (args.length == 3 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.add", Config.locale))) {
                    if (sender instanceof Player) {
                        final Player player = (Player)sender;
                        if (!player.hasPermission(EmeraldBank.prefix + ".add") && !player.isOp()) {
                            return false;
                        }
                    }
                    final OfflinePlayer op2 = PlayerUtil.getOfflinePlayer(args[1].trim());
                    if (op2 == null) {
                        MessageUtil.sendMessage(sender, "error.player.unknown", Config.locale);
                        return true;
                    }
                    try {
                        final int amount = Integer.parseInt(args[2].trim());
                        Balance.add(sender, op2, (double)amount);
                        return true;
                    }
                    catch (NumberFormatException ex) {
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                }
                if (args.length == 3 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.remove", Config.locale))) {
                    if (sender instanceof Player) {
                        final Player player = (Player)sender;
                        if (!player.hasPermission(EmeraldBank.prefix + ".remove") && !player.isOp()) {
                            return false;
                        }
                    }
                    final OfflinePlayer op2 = PlayerUtil.getOfflinePlayer(args[1].trim());
                    if (op2 == null) {
                        MessageUtil.sendMessage(sender, "error.player.unknown", Config.locale);
                        return true;
                    }
                    try {
                        final int amount = Integer.parseInt(args[2].trim());
                        Balance.remove(sender, op2, (double)amount);
                        return true;
                    }
                    catch (NumberFormatException ex) {
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                }
                if (args.length == 3 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.set", Config.locale))) {
                    if (sender instanceof Player) {
                        final Player player = (Player)sender;
                        if (!player.hasPermission(EmeraldBank.prefix + ".set") && !player.isOp()) {
                            return false;
                        }
                    }
                    final OfflinePlayer op2 = PlayerUtil.getOfflinePlayer(args[1].trim());
                    if (op2 == null) {
                        MessageUtil.sendMessage(sender, "error.player.unknown", Config.locale);
                        return true;
                    }
                    try {
                        final int amount = Integer.parseInt(args[2].trim());
                        Balance.set(sender, op2, (double)amount);
                        return true;
                    }
                    catch (NumberFormatException ex) {
                        MessageUtil.sendMessage(sender, "error.invalid.amount", Config.locale);
                        return true;
                    }
                }
                if (args.length == 1 && args[0].trim().equalsIgnoreCase(LocaleStorage.translate("command.help", Config.locale))) {
                    if (!(sender instanceof Player)) {
                        showHelp(sender);
                        return true;
                    }
                    final Player player = (Player)sender;
                    if (player.hasPermission(EmeraldBank.prefix + ".help") || player.isOp()) {
                        showHelp(sender);
                        return true;
                    }
                }
            }
            MessageUtil.sendMessage(sender, "command.bank.usage", Config.locale);
            return true;
        }
        MessageUtil.sendMessage(sender, "warning.command.unknown", Config.locale);
        return true;
    }
    
    private static void showHelp(final CommandSender sender) {
        if (sender instanceof Player) {
            final Player player = (Player)sender;
            MessageUtil.sendMessage(sender, "message.help.bank", Config.locale);
            MessageUtil.sendMessage(sender, (player.hasPermission(EmeraldBank.prefix + ".balance.others") || player.isOp()) ? "message.help.bank_balance.others" : "message.help.bank_balance", Config.locale);
            if (player.hasPermission(EmeraldBank.prefix + ".baltop") || player.isOp()) {
                MessageUtil.sendMessage(sender, "message.help.bank_baltop", Config.locale);
            }
            if (player.hasPermission(EmeraldBank.prefix + ".deposit") || player.isOp()) {
                MessageUtil.sendMessage(sender, "message.help.bank_deposit", Config.locale);
            }
            if (player.hasPermission(EmeraldBank.prefix + ".pay") || player.isOp()) {
                MessageUtil.sendMessage(sender, "message.help.bank_pay", Config.locale);
            }
            if (player.hasPermission(EmeraldBank.prefix + ".transfer") || player.isOp()) {
                MessageUtil.sendMessage(sender, "message.help.bank_transfer", Config.locale);
            }
            if (player.hasPermission(EmeraldBank.prefix + ".withdraw") || player.isOp()) {
                MessageUtil.sendMessage(sender, "message.help.bank_withdraw", Config.locale);
            }
            if (player.hasPermission(EmeraldBank.prefix + ".add")) {
                MessageUtil.sendMessage(sender, "message.help.bank_add", Config.locale);
            }
            if (player.hasPermission(EmeraldBank.prefix + ".remove")) {
                MessageUtil.sendMessage(sender, "message.help.bank_remove", Config.locale);
            }
            if (player.hasPermission(EmeraldBank.prefix + ".set")) {
                MessageUtil.sendMessage(sender, "message.help.bank_set", Config.locale);
            }
        }
        else {
            MessageUtil.sendMessage(sender, "message.help.bank_balance.console", Config.locale);
            MessageUtil.sendMessage(sender, "message.help.bank_add", Config.locale);
            MessageUtil.sendMessage(sender, "message.help.bank_remove", Config.locale);
            MessageUtil.sendMessage(sender, "message.help.bank_set", Config.locale);
        }
        MessageUtil.sendMessage(sender, "message.help.bank_help", Config.locale);
    }
}
