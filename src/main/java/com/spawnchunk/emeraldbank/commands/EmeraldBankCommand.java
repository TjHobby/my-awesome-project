// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.commands;

import com.spawnchunk.emeraldbank.EmeraldBank;
import org.bukkit.entity.Player;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import com.spawnchunk.emeraldbank.util.MessageUtil;
import com.spawnchunk.emeraldbank.config.Config;
import org.bukkit.command.Command;
import org.jetbrains.annotations.NotNull;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class EmeraldBankCommand implements CommandExecutor
{
    public boolean onCommand(@NotNull final CommandSender sender, final Command cmd, @NotNull final String label, @NotNull final String[] args) {
        if (!cmd.getName().equalsIgnoreCase("emeraldbank")) {
            MessageUtil.sendMessage(sender, "warning.command.unknown", Config.locale);
            return true;
        }
        if (args.length == 1 && args[0].equalsIgnoreCase(LocaleStorage.translate("command.reload", Config.locale))) {
            if (sender instanceof Player) {
                final Player player = (Player)sender;
                if (!player.hasPermission(EmeraldBank.prefix + ".reload") && !player.isOp()) {
                    return false;
                }
            }
            EmeraldBank.config.reloadConfig();
            MessageUtil.sendMessage(sender, "message.command.reloaded", Config.locale, EmeraldBank.plugin.getName());
            return true;
        }
        MessageUtil.sendMessage(sender, "command.emeraldbank.usage", Config.locale);
        return true;
    }
}
