// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.commands;

import com.spawnchunk.emeraldbank.modules.Balance;
import com.spawnchunk.emeraldbank.EmeraldBank;
import org.bukkit.entity.Player;
import com.spawnchunk.emeraldbank.util.MessageUtil;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import com.spawnchunk.emeraldbank.config.Config;
import org.bukkit.command.Command;
import org.jetbrains.annotations.NotNull;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;

public class BaltopCommand implements CommandExecutor
{
    public boolean onCommand(@NotNull final CommandSender sender, final Command cmd, @NotNull final String label, @NotNull final String[] args) {
        if (!cmd.getName().equalsIgnoreCase(LocaleStorage.translate("command.baltop", Config.locale))) {
            MessageUtil.sendMessage(sender, "warning.command.unknown", Config.locale);
            return true;
        }
        if (args.length == 0) {
            if (sender instanceof Player) {
                final Player player = (Player)sender;
                if (!player.hasPermission(EmeraldBank.prefix + ".baltop") && !player.isOp()) {
                    return false;
                }
            }
            Balance.baltop(sender);
            return true;
        }
        MessageUtil.sendMessage(sender, "command.baltop.usage", Config.locale);
        return true;
    }
}
