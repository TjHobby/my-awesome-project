// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank;

import com.spawnchunk.emeraldbank.placeholders.MVdWExpansion;
import com.spawnchunk.emeraldbank.placeholders.Expansion;
import java.util.logging.Level;
import com.spawnchunk.emeraldbank.commands.Commands;
import com.spawnchunk.emeraldbank.listeners.CommandListener;
import com.spawnchunk.emeraldbank.modules.FakeAccount;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.ServicePriority;
import com.spawnchunk.emeraldbank.modules.VaultConnector;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import com.spawnchunk.emeraldbank.util.PropertyUtil;
import java.io.File;
import com.spawnchunk.emeraldbank.storage.LocaleStorage;
import java.util.logging.Logger;
import com.spawnchunk.emeraldbank.config.Config;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public final class EmeraldBank extends JavaPlugin implements Listener
{
    public static EmeraldBank plugin;
    public static boolean econ;
    public static Config config;
    public static String level_name;
    public static Logger logger;
    public static String version;
    public static String prefix;
    
    public static void main(final String[] args) {
    }
    
    public void onEnable() {
        EmeraldBank.plugin = this;
        EmeraldBank.logger = EmeraldBank.plugin.getLogger();
        EmeraldBank.prefix = EmeraldBank.plugin.getName().toLowerCase();
        LocaleStorage.loadLocales();
        final File f = new File("server.properties");
        EmeraldBank.level_name = PropertyUtil.getProperty(f, "level-name");
        final String packageName = EmeraldBank.plugin.getServer().getClass().getPackage().getName();
        EmeraldBank.version = packageName.substring(packageName.lastIndexOf(46) + 1);
        EmeraldBank.logger.info(String.format("Using NMS version %s", EmeraldBank.version));
        final String version = EmeraldBank.version;
        switch (version) {
            case "v1_14_R1":
            case "v1_15_R1":
            case "v1_16_R1":
            case "v1_16_R2": {
                if (this.getServer().getPluginManager().getPlugin("Vault") != null) {
                    EmeraldBank.logger.info("Enabled Vault Economy support");
                    Bukkit.getServer().getServicesManager().register((Class)Economy.class, (Object)new VaultConnector(), (Plugin)this, ServicePriority.Highest);
                    EmeraldBank.econ = true;
                }
                this.onServerFullyLoaded();
                EmeraldBank.config = new Config();
                FakeAccount.onEnable();
                this.getServer().getPluginManager().registerEvents((Listener)new CommandListener(), (Plugin)EmeraldBank.plugin);
                Commands.registerCommands();
            }
            default: {
                EmeraldBank.logger.log(Level.SEVERE, "Error! This plugin only supports Spigot versions 1.14+!");
                EmeraldBank.logger.log(Level.SEVERE, "Plugin will be disabled!");
                //Bukkit.getServer().getPluginManager().disablePlugin((Plugin)EmeraldBank.plugin);
            }
        }
    }
    
    public void onDisable() {
        FakeAccount.onDisable();
    }
    
    private void registerExpansion() {
        if (this.setupPlaceholderAPI()) {
            new Expansion(this).register();
            EmeraldBank.logger.info("Registered PlaceholderAPI placeholders");
        }
    }
    
    private void registerMVdW() {
        if (this.setupMVdWPlaceholderAPI()) {
            new MVdWExpansion(this).register();
            EmeraldBank.logger.info("Registered MVdWPlaceholderAPI placeholders");
        }
    }
    
    public void onServerFullyLoaded() {
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask((Plugin)EmeraldBank.plugin, this::registerExpansion);
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask((Plugin)EmeraldBank.plugin, this::registerMVdW);
    }
    
    private boolean setupPlaceholderAPI() {
        final Plugin papi = Bukkit.getPluginManager().getPlugin("PlaceholderAPI");
        if (papi != null && papi.isEnabled()) {
            EmeraldBank.logger.info("Found PlaceholderAPI plugin");
            return true;
        }
        return false;
    }
    
    private boolean setupMVdWPlaceholderAPI() {
        final Plugin mvdw = Bukkit.getPluginManager().getPlugin("MVdWPlaceholderAPI");
        if (mvdw != null && mvdw.isEnabled()) {
            EmeraldBank.logger.info("Found MVdWPlaceholderAPI plugin");
            return true;
        }
        return false;
    }
    
    static {
        EmeraldBank.econ = false;
    }
}
