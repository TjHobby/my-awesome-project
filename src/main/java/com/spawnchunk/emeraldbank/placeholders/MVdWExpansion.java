// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.placeholders;

import org.bukkit.plugin.Plugin;
import be.maximvdw.placeholderapi.PlaceholderAPI;
import org.bukkit.entity.Player;
import com.spawnchunk.emeraldbank.modules.PlayerData;
import org.bukkit.OfflinePlayer;
import com.spawnchunk.emeraldbank.modules.Balance;
import be.maximvdw.placeholderapi.PlaceholderReplaceEvent;
import be.maximvdw.placeholderapi.PlaceholderReplacer;
import com.spawnchunk.emeraldbank.EmeraldBank;

public class MVdWExpansion
{
    private final EmeraldBank plugin;
    private final String identifier;
    
    public MVdWExpansion(final EmeraldBank plugin) {
        this.plugin = plugin;
        this.identifier = plugin.getName().toLowerCase();
    }
    
    public void register() {
        PlaceholderAPI.registerPlaceholder((Plugin)this.plugin, this.identifier + "_balance", (PlaceholderReplacer)new PlaceholderReplacer() {
            public String onPlaceholderReplace(final PlaceholderReplaceEvent event) {
                final Player player = event.getPlayer();
                final double on_hand = Balance.countAll((OfflinePlayer)player);
                final double in_bank = PlayerData.readBalance(player.getUniqueId());
                final double balance = on_hand + in_bank;
                return String.format("%.0f", balance);
            }
        });
        PlaceholderAPI.registerPlaceholder((Plugin)this.plugin, this.identifier + "_in_bank", (PlaceholderReplacer)new PlaceholderReplacer() {
            public String onPlaceholderReplace(final PlaceholderReplaceEvent event) {
                final Player player = event.getPlayer();
                final double in_bank = PlayerData.readBalance(player.getUniqueId());
                return String.format("%.0f", in_bank);
            }
        });
        PlaceholderAPI.registerPlaceholder((Plugin)this.plugin, this.identifier + "_on_hand", (PlaceholderReplacer)new PlaceholderReplacer() {
            public String onPlaceholderReplace(final PlaceholderReplaceEvent event) {
                final Player player = event.getPlayer();
                final double on_hand = Balance.countAll((OfflinePlayer)player);
                return String.format("%.0f", on_hand);
            }
        });
    }
}
