// 
// Decompiled by Procyon v0.5.36
// 

package com.spawnchunk.emeraldbank.placeholders;

import com.spawnchunk.emeraldbank.modules.PlayerData;
import com.spawnchunk.emeraldbank.modules.Balance;
import org.bukkit.OfflinePlayer;
import java.util.Arrays;
import java.util.List;
import com.spawnchunk.emeraldbank.EmeraldBank;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public class Expansion extends PlaceholderExpansion
{
    private final EmeraldBank plugin;
    private final String identifier;
    
    public Expansion(final EmeraldBank plugin) {
        this.plugin = plugin;
        this.identifier = plugin.getName().toLowerCase();
    }
    
    public String getIdentifier() {
        return this.identifier;
    }
    
    public String getAuthor() {
        return "klugemonkey";
    }
    
    public String getVersion() {
        return this.plugin.getDescription().getVersion();
    }
    
    public String getRequiredPlugin() {
        return this.plugin.getName();
    }
    
    public List<String> getPlaceholders() {
        return Arrays.asList(this.placeholder("balance"), this.placeholder("in_bank"), this.placeholder("on_hand"));
    }
    
    public boolean persist() {
        return true;
    }
    
    public String onRequest(final OfflinePlayer player, final String params) {
        if (player == null) {
            return null;
        }
        if (params.equals("balance")) {
            final double on_hand = Balance.countAll(player);
            final double in_bank = PlayerData.readBalance(player.getUniqueId());
            final double balance = on_hand + in_bank;
            return String.format("%.0f", balance);
        }
        if (params.equals("in_bank")) {
            final double in_bank2 = PlayerData.readBalance(player.getUniqueId());
            return String.format("%.0f", in_bank2);
        }
        if (params.equals("on_hand")) {
            final double on_hand = Balance.countAll(player);
            return String.format("%.0f", on_hand);
        }
        return null;
    }
    
    private String placeholder(final String param) {
        return "%" + this.identifier + "_" + param + "%";
    }
}
